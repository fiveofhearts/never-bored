//
//  danishBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "danishBasic.h"
#import "danishDidYouKnow.h"
#import "danishQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface danishBasic ()

@end

@implementation danishBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneDKBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomDKBasic:(id)sender {
    DKBasicTut1.hidden = YES;
    DKBasicTut2.hidden = YES;
    DKBasicTut3.hidden = YES;
    DKBasicTut4.hidden = YES;
    DKBasicIMGPlaceholder.hidden = NO;
    DKBasicPlayButton.hidden = NO;
    DKBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            DKBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            DKBasicTXTPlaceholder.text = @"Farvel";
            break;
        case 1:
            DKBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            DKBasicTXTPlaceholder.text = @"Goddag";
            break;
        case 2:
            DKBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            DKBasicTXTPlaceholder.text = @"Jeg hedder";
            break;
        case 3:
            DKBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            DKBasicTXTPlaceholder.text = @"Det glæder mig at træffe dem";
            break;
        case 4:
            DKBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            DKBasicTXTPlaceholder.text = @"Nej";
            break;
        case 5:
            DKBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            DKBasicTXTPlaceholder.text = @"Tak";
            break;
        case 6:
            DKBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            DKBasicTXTPlaceholder.text = @"Ja";
            break;
        default:
            break;
    }
}

- (IBAction)DKBasicDidYouKnow:(id)sender {
    danishDidYouKnow *DKDidYouKnow = [[danishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKDidYouKnow animated:NO completion:NULL];
}

- (IBAction)DKBasicQuiz:(id)sender {
    danishQuiz *DKQuiz = [[danishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKQuiz animated:NO completion:NULL];
}

- (IBAction)DKPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([DKBasicTXTPlaceholder.text isEqual: @"Farvel"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeDanish", CFSTR ("mp3"), NULL);
    }
    else if ([DKBasicTXTPlaceholder.text isEqual: @"Goddag"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiDanish", CFSTR ("mp3"), NULL);
    }
    else if ([DKBasicTXTPlaceholder.text isEqual: @"Jeg hedder"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsDanish", CFSTR ("mp3"), NULL);
    }
    else if ([DKBasicTXTPlaceholder.text isEqual: @"Det glæder mig at træffe dem"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouDanish", CFSTR ("mp3"), NULL);
    }
    else if ([DKBasicTXTPlaceholder.text isEqual: @"Nej"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noDanish", CFSTR ("mp3"), NULL);
    }
    else if ([DKBasicTXTPlaceholder.text isEqual: @"Tak"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouDanish", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesDanish", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

@end
