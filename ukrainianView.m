//
//  ukrainianView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "ukrainianView.h"
#import "ukrainianDidYouKnow.h"
#import "ukrainianBasic.h"
#import "ukrainianQuiz.h"

@interface ukrainianView ()

@end

@implementation ukrainianView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneUkrainian:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomUkrainian:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        ukrainianDidYouKnow *UADidYouKnow = [[ukrainianDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:UADidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        ukrainianBasic *UABasic = [[ukrainianBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:UABasic animated:NO completion:NULL];
    }
    else {
        ukrainianQuiz *UAQuiz = [[ukrainianQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:UAQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchUADidYouKnow:(id)sender {
    ukrainianDidYouKnow *UADidYouKnow = [[ukrainianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UADidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchUABasic:(id)sender {
    ukrainianBasic *UABasic = [[ukrainianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UABasic animated:NO completion:NULL];
}

- (IBAction)switchUAQuiz:(id)sender {
    ukrainianQuiz *UAQuiz = [[ukrainianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UAQuiz animated:NO completion:NULL];
}
@end
