//
//  learnView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "learnView.h"
#import "spanishView.h"
#import "russianView.h"
#import "germanView.h"
#import "frenchView.h"
#import "turkishView.h"
#import "polishView.h"
#import "ukrainianView.h"
#import "romanianView.h"
#import "dutchView.h"
#import "danishView.h"

@interface learnView ()

@end

@implementation learnView

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)switchSpanish
{
    spanishView *spanish = [[spanishView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:spanish animated:YES completion:NULL];
}

- (void)switchRussian
{
    russianView *russian = [[russianView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:russian animated:YES completion:NULL];
}

- (void)switchGerman
{
    germanView *german = [[germanView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:german animated:YES completion:NULL];
}

- (void)switchFrench
{
    frenchView *french = [[frenchView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:french animated:YES completion:NULL];
}

- (void)switchTurkish
{
    turkishView *turkish = [[turkishView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:turkish animated:YES completion:NULL];
}

- (void)switchPolish
{
    polishView *polish = [[polishView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:polish animated:YES completion:NULL];
}

- (void)switchUkrainian
{
    ukrainianView *ukrainian = [[ukrainianView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ukrainian animated:YES completion:NULL];
}

- (void)switchRomanian
{
    romanianView *romanian = [[romanianView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:romanian animated:YES completion:NULL];
}

- (void)switchDutch
{
    dutchView *dutch = [[dutchView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:dutch animated:YES completion:NULL];
}

- (void)switchDanish
{
    danishView *danish = [[danishView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:danish animated:YES completion:NULL];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 65, 320, 460)];
    int y = 0;
    for (int i=0; i<10; i++) {
        if (i==0) {
            UIButton *spanishButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [spanishButton setImage:[UIImage imageNamed:@"spanishLine.png"] forState:UIControlStateNormal];
            [spanishButton addTarget:self action:@selector(switchSpanish) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:spanishButton];
            
        }
        if (i==1) {
            UIButton *russianButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [russianButton setImage:[UIImage imageNamed:@"russianLine.png"] forState:UIControlStateNormal];
            [russianButton addTarget:self action:@selector(switchRussian) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:russianButton];
            
        }
        if (i==2) {
            UIButton *germanButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [germanButton setImage:[UIImage imageNamed:@"germanLine.png"] forState:UIControlStateNormal];
            [germanButton addTarget:self action:@selector(switchGerman) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:germanButton];
            
        }
        if (i==3) {
            UIButton *frenchButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [frenchButton setImage:[UIImage imageNamed:@"frenchLine.png"] forState:UIControlStateNormal];
            [frenchButton addTarget:self action:@selector(switchFrench) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:frenchButton];
            
        }
        if (i==4) {
            UIButton *turkishButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [turkishButton setImage:[UIImage imageNamed:@"turkishLine.png"] forState:UIControlStateNormal];
            [turkishButton addTarget:self action:@selector(switchTurkish) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:turkishButton];
            
        }
        if (i==5) {
            UIButton *polishButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [polishButton setImage:[UIImage imageNamed:@"polishLine.png"] forState:UIControlStateNormal];
            [polishButton addTarget:self action:@selector(switchPolish) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:polishButton];
            
        }
        if (i==6) {
            UIButton *ukrainianButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [ukrainianButton setImage:[UIImage imageNamed:@"ukrainianLine.png"] forState:UIControlStateNormal];
            [ukrainianButton addTarget:self action:@selector(switchUkrainian) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:ukrainianButton];
            
        }
        if (i==7) {
            UIButton *romanianButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [romanianButton setImage:[UIImage imageNamed:@"romanianLine.png"] forState:UIControlStateNormal];
            [romanianButton addTarget:self action:@selector(switchRomanian) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:romanianButton];
            
        }
        if (i==8) {
            UIButton *dutchButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [dutchButton setImage:[UIImage imageNamed:@"dutchLine.png"] forState:UIControlStateNormal];
            [dutchButton addTarget:self action:@selector(switchDutch) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:dutchButton];
            
        }
        if (i==9) {
            UIButton *danishButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, y, 320, 44)];
            [danishButton setImage:[UIImage imageNamed:@"danishLine.png"] forState:UIControlStateNormal];
            [danishButton addTarget:self action:@selector(switchDanish) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:danishButton];
            
        }
        y=y+45;
    }
    [scrollView setContentSize:CGSizeMake(320, 568)];
    [self.view addSubview:scrollView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneLearn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomLearn:(id)sender {
    int i = arc4random() %10;
    if (i == 1) {
        spanishView *spanish = [[spanishView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:spanish animated:YES completion:NULL];
    }
    else if (i == 2) {
        russianView *russian = [[russianView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:russian animated:YES completion:NULL];
    }
    else if (i == 3) {
        germanView *german = [[germanView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:german animated:YES completion:NULL];
    }
    else if (i == 4) {
        frenchView *french = [[frenchView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:french animated:YES completion:NULL];
    }
    else if (i == 5) {
        turkishView *turkish = [[turkishView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:turkish animated:YES completion:NULL];
    }
    else if (i == 6) {
        polishView *polish = [[polishView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:polish animated:YES completion:NULL];
    }
    else if (i == 7) {
        ukrainianView *ukrainian = [[ukrainianView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:ukrainian animated:YES completion:NULL];
    }
    else if (i == 8) {
        romanianView *romanian = [[romanianView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:romanian animated:YES completion:NULL];
    }
    else if (i == 9) {
        dutchView *dutch = [[dutchView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:dutch animated:YES completion:NULL];
    }
    else {
        danishView *danish = [[danishView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:danish animated:YES completion:NULL];
    }
}

@end
