//
//  russianBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "russianBasic.h"
#import "russianDidYouKnow.h"
#import "russianQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface russianBasic ()

@end

@implementation russianBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneRUBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomRUBasic:(id)sender {
    RUBasicTut1.hidden = YES;
    RUBasicTut2.hidden = YES;
    RUBasicTut3.hidden = YES;
    RUBasicTut4.hidden = YES;
    RUBasicIMGPlaceholder.hidden = NO;
    RUBasicPlayButton.hidden = NO;
    RUBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            RUBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            RUBasicTXTPlaceholder.text = @"Дο свидания";
            break;
        case 1:
            RUBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            RUBasicTXTPlaceholder.text = @"Здравствуйте";
            break;
        case 2:
            RUBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            RUBasicTXTPlaceholder.text = @"Меня зoвут";
            break;
        case 3:
            RUBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            RUBasicTXTPlaceholder.text = @"Очень приятно";
            break;
        case 4:
            RUBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            RUBasicTXTPlaceholder.text = @"Нет";
            break;
        case 5:
            RUBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            RUBasicTXTPlaceholder.text = @"Спасибо";
            break;
        case 6:
            RUBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            RUBasicTXTPlaceholder.text = @"Да";
            break;
        default:
            break;
    }
}

- (IBAction)RUBasicDidYouKnow:(id)sender {
    russianDidYouKnow *RUDidYouKnow = [[russianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUDidYouKnow animated:NO completion:NULL];
}

- (IBAction)RUBasicQuiz:(id)sender {
    russianQuiz *RUQuiz = [[russianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUQuiz animated:NO completion:NULL];
}

- (IBAction)RUPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([RUBasicTXTPlaceholder.text isEqual: @"Дο свидания"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeRussian", CFSTR ("mp3"), NULL);
    }
    else if ([RUBasicTXTPlaceholder.text isEqual: @"Здравствуйте"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiRussian", CFSTR ("mp3"), NULL);
    }
    else if ([RUBasicTXTPlaceholder.text isEqual: @"Меня зoвут"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsRussian", CFSTR ("mp3"), NULL);
    }
    else if ([RUBasicTXTPlaceholder.text isEqual: @"Очень приятно"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouRussian", CFSTR ("mp3"), NULL);
    }
    else if ([RUBasicTXTPlaceholder.text isEqual: @"Нет"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noRussian", CFSTR ("mp3"), NULL);
    }
    else if ([RUBasicTXTPlaceholder.text isEqual: @"Спасибо"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouRussian", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesRussian", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end