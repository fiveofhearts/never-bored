//
//  polishQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "polishQuiz.h"
#import "polishDidYouKnow.h"
#import "polishBasic.h"

@interface polishQuiz ()

@end

@implementation polishQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTRQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomTRQuiz:(id)sender {
    TRQuizTut1.hidden = YES;
    TRQuizTut2.hidden = YES;
    TRQuizTut3.hidden = YES;
    TRQuizTut4.hidden = YES;
    TRQuizQuestion.hidden = NO;
    TRQuizButtonA.hidden = NO;
    TRQuizButtonB.hidden = NO;
    TRQuizButtonC.hidden = NO;
    TRQuizButtonD.hidden = NO;
    TRQuizAnswer1Label.hidden = NO;
    TRQuizAnswer2Label.hidden = NO;
    TRQuizAnswer3Label.hidden = NO;
    TRQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            TRQuizQuestion.text = @"Poland’s national symbol is the ... -tailed Eagle.";
            TRQuizAnswer1Label.text = @"White";
            TRQuizAnswer2Label.text = @"Red";
            TRQuizAnswer3Label.text = @"Blue";
            TRQuizAnswer4Label.text = @"Yellow";
            TRQuizResult.text = @"";
            break;
        case 1:
            TRQuizQuestion.text = @"The most popular dog’s name in Poland is “Burek” which is actually the Polish word for a ... colour.";
            TRQuizAnswer1Label.text = @"light-brown";
            TRQuizAnswer2Label.text = @"brown-grey";
            TRQuizAnswer3Label.text = @"light-orange";
            TRQuizAnswer4Label.text = @"dark-yellow";
            TRQuizResult.text = @"";
            break;
        case 2:
            TRQuizQuestion.text = @"Some Polish beer is ... alcohol.";
            TRQuizAnswer1Label.text = @"8%";
            TRQuizAnswer2Label.text = @"9%";
            TRQuizAnswer3Label.text = @"10%";
            TRQuizAnswer4Label.text = @"11%";
            TRQuizResult.text = @"";
            break;
        case 3:
            TRQuizQuestion.text = @"... % of the land is dedicated to farming.";
            TRQuizAnswer1Label.text = @"40%";
            TRQuizAnswer2Label.text = @"60%";
            TRQuizAnswer3Label.text = @"70%";
            TRQuizAnswer4Label.text = @"50%";
            TRQuizResult.text = @"";
            break;
        case 4:
            TRQuizQuestion.text = @"Nazywam się means ... in Polish.";
            TRQuizAnswer1Label.text = @"Hello";
            TRQuizAnswer2Label.text = @"Goodbye";
            TRQuizAnswer3Label.text = @"Yes";
            TRQuizAnswer4Label.text = @"My name is";
            TRQuizResult.text = @"";
            break;
        case 5:
            TRQuizQuestion.text = @"Cześć means ... in Polish.";
            TRQuizAnswer1Label.text = @"Goodbye";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Hello";
            TRQuizAnswer4Label.text = @"No";
            TRQuizResult.text = @"";
            break;
        case 6:
            TRQuizQuestion.text = @"Dziękuję means ... in Polish.";
            TRQuizAnswer1Label.text = @"Yes";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Goodbye";
            TRQuizAnswer4Label.text = @"Hello";
            TRQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)TRQuizDidYouKnow:(id)sender {
    polishDidYouKnow *PLDidYouKnow = [[polishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLDidYouKnow animated:NO completion:NULL];
}

- (IBAction)TRQuizBasic:(id)sender {
    polishBasic *PLBasic = [[polishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLBasic animated:NO completion:NULL];
}

- (IBAction)TRQuizAnswer1:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Poland’s national symbol is the ... -tailed Eagle."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"The most popular dog’s name in Poland is “Burek” which is actually the Polish word for a ... colour."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Some Polish beer is ... alcohol."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"... % of the land is dedicated to farming."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Nazywam się means ... in Polish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Cześć means ... in Polish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer2:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Poland’s national symbol is the ... -tailed Eagle."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The most popular dog’s name in Poland is “Burek” which is actually the Polish word for a ... colour."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Some Polish beer is ... alcohol."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"... % of the land is dedicated to farming."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Nazywam się means ... in Polish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Cześć means ... in Polish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)TRQuizAnswer3:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Poland’s national symbol is the ... -tailed Eagle."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The most popular dog’s name in Poland is “Burek” which is actually the Polish word for a ... colour."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Some Polish beer is ... alcohol."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"... % of the land is dedicated to farming."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Nazywam się means ... in Polish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Cześć means ... in Polish."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer4:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Poland’s national symbol is the ... -tailed Eagle."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The most popular dog’s name in Poland is “Burek” which is actually the Polish word for a ... colour."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Some Polish beer is ... alcohol."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"... % of the land is dedicated to farming."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Nazywam się means ... in Polish."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Cześć means ... in Polish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
}
@end
