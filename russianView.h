//
//  russianView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface russianView : UIViewController {
}
- (IBAction)doneRussian:(id)sender;
- (IBAction)randomRussian:(id)sender;
- (IBAction)switchRUDidYouKnow:(id)sender;
- (IBAction)switchRUBasic:(id)sender;
- (IBAction)switchRUQuiz:(id)sender;
@end
