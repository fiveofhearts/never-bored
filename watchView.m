//
//  watchView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "watchView.h"

@interface watchView ()

@end

@implementation watchView


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.watchTitle = [[MarqueeLabel alloc] initWithFrame:CGRectMake(0, 72, 320, 44) rate:50.0f andFadeLength:10.0f];
    self.watchTitle.numberOfLines = 1;
    self.watchTitle.shadowOffset = CGSizeMake(0.0, -1.0);
    self.watchTitle.textAlignment = NSTextAlignmentLeft;
    self.watchTitle.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:25.000];
    self.watchTitle.marqueeType = MLLeftRight;
    [self.view addSubview:self.watchTitle];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneWatch:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomWatch:(id)sender{
    self.watchTitle.hidden = NO;
    watchTut1.hidden = YES;
    watchTut2.hidden = YES;
    watchTut3.hidden = YES;
    watchTut4.hidden = YES;
    watchTut5.hidden = YES;
    int i = rand() % 21;
    switch (i) {
        case 0:
            self.watchTitle.text = @" How To Catch a Pokemon";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/NyGv8XtKJc4\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 1:
            self.watchTitle.text = @" The Force: Volkswagen Commercial";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/R55e-uHQna0\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 2:
            self.watchTitle.text = @" Evolution of Dance - By Judson Laipply";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/dMH0bHeiRNg\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 3:
            self.watchTitle.text = @" Paul Potts sings Nessun Dorma";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/1k08yxu57NA\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 4:
            self.watchTitle.text = @" How Animals Eat Their Food | MisterEpicMann";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/qnydFmqHuVo\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 5:
            self.watchTitle.text = @" PEOPLE ARE AWESOME (Hadouken! - Mecha Love)";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/Vo0Cazxj_yc\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 6:
            self.watchTitle.text = @" Dove Real Beauty Sketches";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/XpaOjMXyJGk\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 7:
            self.watchTitle.text = @" Where the Hell is Matt? 2012";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/Pwe-pA6TaZk\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 8:
            self.watchTitle.text = @" Giant 6ft Water Balloon - The Slow Mo Guys";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/j_OyHUqIIOU\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 9:
            self.watchTitle.text = @" Pepsi MAX & Jeff Gordon Present: \"Test Drive\"";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/Q5mHPo2yDG8\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 10:
            self.watchTitle.text = @" The Balloonery - 2500 balloons - best office prank balloon room";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/T0Y_PVF_EVg\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 11:
            self.watchTitle.text = @" Pepsi MAX & Kyrie Irving Present: \"Uncle Drew\"";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/8DnKOc6FISU\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 12:
            self.watchTitle.text = @" freddiew : Future First Person Shooter";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/CyCyzB0CedM\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 13:
            self.watchTitle.text = @" Fruit Ninja in Real Life to Dubstep!";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/w-llZmPPNwU\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 14:
            self.watchTitle.text = @" How it Feels [through Google Glass]";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/v1uyQZNg2vE\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 15:
            self.watchTitle.text = @" TWO DOGS DINING";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/EVwlMVYqMu4\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 16:
            self.watchTitle.text = @" Attraction perform their stunning shadow act - Week 1 Auditions | Britain's Got Talent 2013";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/a4Fv98jttYA\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 17:
            self.watchTitle.text = @" Drifting Motorbike - Drift Gymkhana - Jorian Ponomareff";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/20XsaHpRQC8\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 18:
            self.watchTitle.text = @" Despicable Me - Mini-Movie 'Banana' Preview";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/BYBw_o_2nG0\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 19:
            self.watchTitle.text = @" How to make Hot Ice!!! Crazy";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/aC-KOYQsIvU\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        case 20:
            self.watchTitle.text = @" Michael Jackson Suprise on Britains Got Talent Suleman Mirza";
            [[self watchPlaceholder]loadHTMLString:@"<iframe width=\"300\" height=\"225\" src=\"http://www.youtube.com/embed/9Qj7Y0qXJZ4\" frameborder=\"0\" allowfullscreen></iframe>" baseURL:nil];
            break;
        default:
            break;
    }
}

@end
