//
//  spanishQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "spanishQuiz.h"
#import "spanishDidYouKnow.h"
#import "spanishBasic.h"

@interface spanishQuiz ()

@end

@implementation spanishQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneESQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomESQuiz:(id)sender {
    ESQuizTut1.hidden = YES;
    ESQuizTut2.hidden = YES;
    ESQuizTut3.hidden = YES;
    ESQuizTut4.hidden = YES;
    ESQuizQuestion.hidden = NO;
    ESQuizButtonA.hidden = NO;
    ESQuizButtonB.hidden = NO;
    ESQuizButtonC.hidden = NO;
    ESQuizButtonD.hidden = NO;
    ESQuizAnswer1Label.hidden = NO;
    ESQuizAnswer2Label.hidden = NO;
    ESQuizAnswer3Label.hidden = NO;
    ESQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            ESQuizQuestion.text = @"How many times is Spain bigger than UK?";
            ESQuizAnswer1Label.text = @"5";
            ESQuizAnswer2Label.text = @"4";
            ESQuizAnswer3Label.text = @"3";
            ESQuizAnswer4Label.text = @"2";
            ESQuizResult.text = @"";
            break;
        case 1:
            ESQuizQuestion.text = @"What's the average life expectancy in Spain?";
            ESQuizAnswer1Label.text = @"62";
            ESQuizAnswer2Label.text = @"79";
            ESQuizAnswer3Label.text = @"83";
            ESQuizAnswer4Label.text = @"91";
            ESQuizResult.text = @"";
            break;
        case 2:
            ESQuizQuestion.text = @"The tooth animal called Ratoncito Perez is ...";
            ESQuizAnswer1Label.text = @"a horse";
            ESQuizAnswer2Label.text = @"a deer";
            ESQuizAnswer3Label.text = @"a mouse";
            ESQuizAnswer4Label.text = @"a bunny";
            ESQuizResult.text = @"";
            break;
        case 3:
            ESQuizQuestion.text = @"How many km of beaches does Spain have?";
            ESQuizAnswer1Label.text = @"9000";
            ESQuizAnswer2Label.text = @"7000";
            ESQuizAnswer3Label.text = @"6000";
            ESQuizAnswer4Label.text = @"8000";
            ESQuizResult.text = @"";
            break;
        case 4:
            ESQuizQuestion.text = @"Me llamo means ... in Spanish.";
            ESQuizAnswer1Label.text = @"Hello";
            ESQuizAnswer2Label.text = @"Goodbye";
            ESQuizAnswer3Label.text = @"Yes";
            ESQuizAnswer4Label.text = @"My name is";
            ESQuizResult.text = @"";
            break;
        case 5:
            ESQuizQuestion.text = @"Hola means ... in Spanish.";
            ESQuizAnswer1Label.text = @"Goodbye";
            ESQuizAnswer2Label.text = @"Thank you";
            ESQuizAnswer3Label.text = @"Hello";
            ESQuizAnswer4Label.text = @"No";
            ESQuizResult.text = @"";
            break;
        case 6:
            ESQuizQuestion.text = @"Gracias means ... in Spanish.";
            ESQuizAnswer1Label.text = @"Yes";
            ESQuizAnswer2Label.text = @"Thank you";
            ESQuizAnswer3Label.text = @"Goodbye";
            ESQuizAnswer4Label.text = @"Hello";
            ESQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)ESQuizDidYouKnow:(id)sender {
    spanishDidYouKnow *ESDidYouKnow = [[spanishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESDidYouKnow animated:NO completion:NULL];
}

- (IBAction)ESQuizBasic:(id)sender {
    spanishBasic *ESBasic = [[spanishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESBasic animated:NO completion:NULL];
}

- (IBAction)ESQuizAnswer1:(id)sender {
    if ([ESQuizQuestion.text isEqual: @"How many times is Spain bigger than UK?"]) {
        ESQuizResult.text = @"Correct!";
        ESQuizResult.textColor = [UIColor greenColor];
    }
    else if ([ESQuizQuestion.text isEqual: @"What's the average life expectancy in Spain?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer1Label.hidden = YES;
        ESQuizButtonA.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"The tooth animal called Ratoncito Perez is ..."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer1Label.hidden = YES;
        ESQuizButtonA.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"How many km of beaches does Spain have?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer1Label.hidden = YES;
        ESQuizButtonA.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"Me llamo means ... in Spanish."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer1Label.hidden = YES;
        ESQuizButtonA.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"Hola means ... in Spanish."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer1Label.hidden = YES;
        ESQuizButtonA.hidden = YES;
    }
    else {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer1Label.hidden = YES;
        ESQuizButtonA.hidden = YES;
    }
}

- (IBAction)ESQuizAnswer2:(id)sender {
    if ([ESQuizQuestion.text isEqual: @"How many times is Spain bigger than UK?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer2Label.hidden = YES;
        ESQuizButtonB.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"What's the average life expectancy in Spain?"]) {
        ESQuizResult.text = @"Good. Well done!!!";
        ESQuizResult.textColor = [UIColor greenColor];
    }
    else if ([ESQuizQuestion.text isEqual: @"The tooth animal called Ratoncito Perez is ..."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer2Label.hidden = YES;
        ESQuizButtonB.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"How many km of beaches does Spain have?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer2Label.hidden = YES;
        ESQuizButtonB.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"Me llamo means ... in Spanish."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer2Label.hidden = YES;
        ESQuizButtonB.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"Hola means ... in Spanish."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer2Label.hidden = YES;
        ESQuizButtonB.hidden = YES;
    }
    else {
        ESQuizResult.text = @"Good. Well done!!!";
        ESQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)ESQuizAnswer3:(id)sender {
    if ([ESQuizQuestion.text isEqual: @"How many times is Spain bigger than UK?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer3Label.hidden = YES;
        ESQuizButtonC.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"What's the average life expectancy in Spain?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer3Label.hidden = YES;
        ESQuizButtonC.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"The tooth animal called Ratoncito Perez is ..."]) {
        ESQuizResult.text = @"Good. Well done!!!";
        ESQuizResult.textColor = [UIColor greenColor];
    }
    else if ([ESQuizQuestion.text isEqual: @"How many km of beaches does Spain have?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer3Label.hidden = YES;
        ESQuizButtonC.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"Me llamo means ... in Spanish."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer3Label.hidden = YES;
        ESQuizButtonC.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"Hola means ... in Spanish."]) {
        ESQuizResult.text = @"Good. Well done!!!";
        ESQuizResult.textColor = [UIColor greenColor];
    }
    else {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer3Label.hidden = YES;
        ESQuizButtonC.hidden = YES;
    }
}

- (IBAction)ESQuizAnswer4:(id)sender {
    if ([ESQuizQuestion.text isEqual: @"How many times is Spain bigger than UK?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer4Label.hidden = YES;
        ESQuizButtonD.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"What's the average life expectancy in Spain?"]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer4Label.hidden = YES;
        ESQuizButtonD.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"The tooth animal called Ratoncito Perez is ..."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer4Label.hidden = YES;
        ESQuizButtonD.hidden = YES;
    }
    else if ([ESQuizQuestion.text isEqual: @"How many km of beaches does Spain have?"]) {
        ESQuizResult.text = @"Good. Well done!!!";
        ESQuizResult.textColor = [UIColor greenColor];
    }
    else if ([ESQuizQuestion.text isEqual: @"Me llamo means ... in Spanish."]) {
        ESQuizResult.text = @"Good. Well done!!!";
        ESQuizResult.textColor = [UIColor greenColor];
    }
    else if ([ESQuizQuestion.text isEqual: @"Hola means ... in Spanish."]) {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer4Label.hidden = YES;
        ESQuizButtonD.hidden = YES;
    }
    else {
        ESQuizResult.text = @"Wrong!";
        ESQuizResult.textColor = [UIColor redColor];
        ESQuizAnswer4Label.hidden = YES;
        ESQuizButtonD.hidden = YES;
    }
}
@end