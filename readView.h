//
//  readView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface readView : UIViewController {
    __weak IBOutlet UILabel *readTitle;
    __weak IBOutlet UITextView *readPlaceholder;
    __weak IBOutlet UILabel *readTut1;
    __weak IBOutlet UIButton *readTut2;
    __weak IBOutlet UILabel *readTut3;
    __weak IBOutlet UILabel *readTut4;
}
- (IBAction)doneRead:(id)sender;
- (IBAction)randomRead:(id)sender;
@end
