//
//  frenchBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "frenchBasic.h"
#import "frenchDidYouKnow.h"
#import "frenchQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface frenchBasic ()

@end

@implementation frenchBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneFRBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomFRBasic:(id)sender {
    FRBasicTut1.hidden = YES;
    FRBasicTut2.hidden = YES;
    FRBasicTut3.hidden = YES;
    FRBasicTut4.hidden = YES;
    FRBasicIMGPlaceholder.hidden = NO;
    FRBasicPlayButton.hidden = NO;
    FRBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            FRBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            FRBasicTXTPlaceholder.text = @"Au revoir";
            break;
        case 1:
            FRBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            FRBasicTXTPlaceholder.text = @"Bonjour";
            break;
        case 2:
            FRBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            FRBasicTXTPlaceholder.text = @"Je m'appelle";
            break;
        case 3:
            FRBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            FRBasicTXTPlaceholder.text = @"Enchanté";
            break;
        case 4:
            FRBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            FRBasicTXTPlaceholder.text = @"Non";
            break;
        case 5:
            FRBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            FRBasicTXTPlaceholder.text = @"Merci";
            break;
        case 6:
            FRBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            FRBasicTXTPlaceholder.text = @"Oui";
            break;
        default:
            break;
    }
}

- (IBAction)FRBasicDidYouKnow:(id)sender {
    frenchDidYouKnow *FRDidYouKnow = [[frenchDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRDidYouKnow animated:NO completion:NULL];
}

- (IBAction)FRBasicQuiz:(id)sender {
    frenchQuiz *FRQuiz = [[frenchQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRQuiz animated:NO completion:NULL];
}

- (IBAction)FRPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([FRBasicTXTPlaceholder.text isEqual: @"Au revoir"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeFrench", CFSTR ("mp3"), NULL);
    }
    else if ([FRBasicTXTPlaceholder.text isEqual: @"Bonjour"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiFrench", CFSTR ("mp3"), NULL);
    }
    else if ([FRBasicTXTPlaceholder.text isEqual: @"Je m'appelle"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsFrench", CFSTR ("mp3"), NULL);
    }
    else if ([FRBasicTXTPlaceholder.text isEqual: @"Enchanté"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouFrench", CFSTR ("mp3"), NULL);
    }
    else if ([FRBasicTXTPlaceholder.text isEqual: @"Non"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noFrench", CFSTR ("mp3"), NULL);
    }
    else if ([FRBasicTXTPlaceholder.text isEqual: @"Merci"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouFrench", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesFrench", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end