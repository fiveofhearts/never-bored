//
//  danishDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "danishDidYouKnow.h"
#import "danishBasic.h"
#import "danishQuiz.h"

@interface danishDidYouKnow ()

@end

@implementation danishDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneDKDidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomDKDidYouKNow:(id)sender {
    DKDidYouKnowTut1.hidden = YES;
    DKDidYouKnowTut2.hidden = YES;
    DKDidYouKnowTut3.hidden = YES;
    DKDidYouKnowTut4.hidden = YES;
    DKDidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            DKDidYouKnowPlaceholder.text = @"Fact #1 \n \nDanish men marry the oldest of all Europeans - at 32 years old in average.";
            break;
        case 1:
            DKDidYouKnowPlaceholder.text = @"Fact #2 \n \nThe country's average height above sea level is only 31 metres and the highest natural point is Møllehøj, at 170.86 metres.";
            break;
        case 2:
            DKDidYouKnowPlaceholder.text = @"Fact #3 \n \nSeparate studies have ranked Danish people as the happiest in the EU (2007 Cambridge University study), and happiest people in the world (2006 Leicester University study) or 2nd happiest in the world (World Database of Happiness 2000-2009).";
            break;
        case 3:
            DKDidYouKnowPlaceholder.text = @"Fact #4 \n \nDenmark has had no less than 14 Nobel laureates, including 4 in Literature, 5 in Physiology or Medicine, and one Peace prize. With its population of about 5 million, it is one of the highest per capita ratio of any country in the world.";
            break;
        case 4:
            DKDidYouKnowPlaceholder.text = @"Fact #5 \n \nThe Danish royal family is probably the oldest uninterrupted European monarchy. It traces back its roots to legendary kings in the Antiquity. Gorm the Old, the first king of the 'official line', ruled from 934 C.E.";
            break;
        case 5:
            DKDidYouKnowPlaceholder.text = @"Fact #6 \n \nThe Danish prince Hamlet, the fictional character of William Shakespeare's famous play, was inspired by an old Danish myth of the Viking Prince Amled of Jutland.";
            break;
        case 6:
            DKDidYouKnowPlaceholder.text = @"Fact #7 \n \nCarlsberg, Tuborg and Lego are danish companies.";
            break;
        default:
            break;
    }
}

- (IBAction)DKDidYouKnowBasic:(id)sender {
    danishBasic *DKBasic = [[danishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKBasic animated:NO completion:NULL];
}

- (IBAction)DKDidYouKnowQuiz:(id)sender {
    danishQuiz *DKQuiz = [[danishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKQuiz animated:NO completion:NULL];
}
@end
