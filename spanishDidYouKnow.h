//
//  spanishDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface spanishDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *ESDidYouKnowPlaceholder;
    __weak IBOutlet UILabel *ESDidYouKnowTut1;
    __weak IBOutlet UIButton *ESDidYouKnowTut2;
    __weak IBOutlet UILabel *ESDidYouKnowTut3;
    __weak IBOutlet UILabel *ESDidYouKnowTut4;
    
}
- (IBAction)doneESDidYouKnow:(id)sender;
- (IBAction)randomESDidYouKNow:(id)sender;
- (IBAction)ESDidYouKnowBasic:(id)sender;
- (IBAction)ESDidYouKnowQuiz:(id)sender;
@end
