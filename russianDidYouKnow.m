//
//  russianDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "russianDidYouKnow.h"
#import "russianBasic.h"
#import "russianQuiz.h"

@interface russianDidYouKnow ()

@end

@implementation russianDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneRUDidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomRUDidYouKNow:(id)sender {
    RUDidYouKnowTut1.hidden = YES;
    RUDidYouKnowTut2.hidden = YES;
    RUDidYouKnowTut3.hidden = YES;
    RUDidYouKnowTut4.hidden = YES;
    RUDidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            RUDidYouKnowPlaceholder.text = @"Fact #1 \n \nThere are 100 reserves and 35 national parks in Russia.";
            break;
        case 1:
            RUDidYouKnowPlaceholder.text = @"Fact #2 \n \nRussia is the only State, on the territory of which there are 12 seas.";
            break;
        case 2:
            RUDidYouKnowPlaceholder.text = @"Fact #3 \n \nWomen make 46.9% of the employed population in Russia.";
            break;
        case 3:
            RUDidYouKnowPlaceholder.text = @"Fact #4 \n \nThe Trans-Siberian Railway is the longest railway in the world, and crosses nearly all of Russia, the world’s largest country by area. At approximately 9200 kilometers, or 5700 miles, the train leaves Moscow, located in European Russia, crosses into Asia, and reaches the Pacific Ocean port of Vladivostok.";
            break;
        case 4:
            RUDidYouKnowPlaceholder.text = @"Fact #5 \n \nEvery day over 9 MILLION passengers ride the Metro, Moscow’s vast underground subway system. In a single day, an average of 9915 trains operate between 5am – 1am, making the New York subway system seem like a miniature child’s toy.";
            break;
        case 5:
            RUDidYouKnowPlaceholder.text = @"Fact #6 \n \nApart from traditional New Year there is another celebration in Russia called The Old New Year or the Orthodox New Year which is an informal traditional Orthodox holiday, celebrated as the start of the New Year by the Julian calendar. In the 20th and 21st centuries, the Old New Year falls on January 14 in the Gregorian calendar, 13 days after its New Year.";
            break;
        case 6:
            RUDidYouKnowPlaceholder.text = @"Fact #7 \n \nIn 2005, the regional governor of Ulyanovsk, an area in central Russia, decided to deal directly with his country’s dwindling population. Governor Sergi Morozov introduced September 12 as a “Day of Conception”, which grants couples a half day off from work to procreate. Nine months later, women who give birth closest to June 12—Russia’s National Day—receive prizes which include cars, cash, and appliances.";
            break;
        default:
            break;
    }
}

- (IBAction)RUDidYouKnowBasic:(id)sender {
    russianBasic *RUBasic = [[russianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUBasic animated:NO completion:NULL];
}

- (IBAction)RUDidYouKnowQuiz:(id)sender {
    russianQuiz *RUQuiz = [[russianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUQuiz animated:NO completion:NULL];
}
@end