//
//  russianQuiz.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface russianQuiz : UIViewController {
    __weak IBOutlet UITextView *RUQuizQuestion;
    __weak IBOutlet UILabel *RUQuizAnswer1Label;
    __weak IBOutlet UILabel *RUQuizAnswer2Label;
    __weak IBOutlet UILabel *RUQuizAnswer3Label;
    __weak IBOutlet UILabel *RUQuizAnswer4Label;
    __weak IBOutlet UILabel *RUQuizResult;
    __weak IBOutlet UIButton *RUQuizButtonA;
    __weak IBOutlet UIButton *RUQuizButtonB;
    __weak IBOutlet UIButton *RUQuizButtonC;
    __weak IBOutlet UIButton *RUQuizButtonD;
    __weak IBOutlet UILabel *RUQuizTut1;
    __weak IBOutlet UIButton *RUQuizTut2;
    __weak IBOutlet UILabel *RUQuizTut3;
    __weak IBOutlet UILabel *RUQuizTut4;
}
- (IBAction)doneRUQuiz:(id)sender;
- (IBAction)randomRUQuiz:(id)sender;
- (IBAction)RUQuizDidYouKnow:(id)sender;
- (IBAction)RUQuizBasic:(id)sender;
- (IBAction)RUQuizAnswer1:(id)sender;
- (IBAction)RUQuizAnswer2:(id)sender;
- (IBAction)RUQuizAnswer3:(id)sender;
- (IBAction)RUQuizAnswer4:(id)sender;

@end
