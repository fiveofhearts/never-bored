//
//  turkishDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "turkishDidYouKnow.h"
#import "turkishBasic.h"
#import "turkishQuiz.h"

@interface turkishDidYouKnow ()

@end

@implementation turkishDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTRDidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomTRDidYouKNow:(id)sender {
    TRDidYouKnowTut1.hidden = YES;
    TRDidYouKnowTut2.hidden = YES;
    TRDidYouKnowTut3.hidden = YES;
    TRDidYouKnowTut4.hidden = YES;
    TRDidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            TRDidYouKnowPlaceholder.text = @"Fact #1 \n \nIstanbul is the only city in the world that straddles two continents: Asia and Europe.";
            break;
        case 1:
            TRDidYouKnowPlaceholder.text = @"Fact #2 \n \nTurkey provided homes for some 313,000 Bulgarian refugees of Turkish origin expelled from their homelands in Bulgaria in 1989.";
            break;
        case 2:
            TRDidYouKnowPlaceholder.text = @"Fact #3 \n \nSmallpox vaccination was introduced to England and Europe from Turkey by Lady Montagu in early 18th century (after Turkish phsycians saved her son's life).";
            break;
        case 3:
            TRDidYouKnowPlaceholder.text = @"Fact #4 \n \nAlexander the Great conquered a large territory in what is now Turkey - and cut the Gordion Knot in the Phrygian capital (Gordium) not far from Turkey's present-day capital (Ankara).";
            break;
        case 4:
            TRDidYouKnowPlaceholder.text = @"Fact #5 \n \nLeonardo da Vinci drew designs for a bridge over the Bosphorus, the strait that flows through Europe and Asia. (It was never built then; but now there are two Bosphorus bridges.).";
            break;
        case 5:
            TRDidYouKnowPlaceholder.text = @"Fact #6 \n \nOne of the biggest and best preserved theatres of antiquity seating 15,000 is Aspendos on the southern coast of Turkey where international music festivals are held each year.";
            break;
        case 6:
            TRDidYouKnowPlaceholder.text = @"Fact #7 \n \nTurks gave the Dutch their famous tulips.";
            break;
        default:
            break;
    }
}

- (IBAction)TRDidYouKnowBasic:(id)sender {
    turkishBasic *TRBasic = [[turkishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRBasic animated:NO completion:NULL];
}

- (IBAction)TRDidYouKnowQuiz:(id)sender {
    turkishQuiz *TRQuiz = [[turkishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRQuiz animated:NO completion:NULL];
}
@end
