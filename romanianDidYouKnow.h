//
//  romanianDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface romanianDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *RODidYouKnowPlaceholder;
    __weak IBOutlet UILabel *RODidYouKnowTut1;
    __weak IBOutlet UIButton *RODidYouKnowTut2;
    __weak IBOutlet UILabel *RODidYouKnowTut3;
    __weak IBOutlet UILabel *RODidYouKnowTut4;
    
}
- (IBAction)doneRODidYouKnow:(id)sender;
- (IBAction)randomRODidYouKNow:(id)sender;
- (IBAction)RODidYouKnowBasic:(id)sender;
- (IBAction)RODidYouKnowQuiz:(id)sender;
@end
