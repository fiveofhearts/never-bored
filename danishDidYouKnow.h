//
//  danishDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface danishDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *DKDidYouKnowPlaceholder;
    __weak IBOutlet UILabel *DKDidYouKnowTut1;
    __weak IBOutlet UIButton *DKDidYouKnowTut2;
    __weak IBOutlet UILabel *DKDidYouKnowTut3;
    __weak IBOutlet UILabel *DKDidYouKnowTut4;
    
}
- (IBAction)doneDKDidYouKnow:(id)sender;
- (IBAction)randomDKDidYouKNow:(id)sender;
- (IBAction)DKDidYouKnowBasic:(id)sender;
- (IBAction)DKDidYouKnowQuiz:(id)sender;
@end
