//
//  spanishBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "spanishBasic.h"
#import "spanishDidYouKnow.h"
#import "spanishQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface spanishBasic ()

@end

@implementation spanishBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneESBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomESBasic:(id)sender {
    ESBasicTut1.hidden = YES;
    ESBasicTut2.hidden = YES;
    ESBasicTut3.hidden = YES;
    ESBasicTut4.hidden = YES;
    ESBasicIMGPlaceholder.hidden = NO;
    ESBasicPlayButton.hidden = NO;
    ESBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            ESBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            ESBasicTXTPlaceholder.text = @"Adiós";
            break;
        case 1:
            ESBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            ESBasicTXTPlaceholder.text = @"Hola";
            break;
        case 2:
            ESBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            ESBasicTXTPlaceholder.text = @"Me llamo";
            break;
        case 3:
            ESBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            ESBasicTXTPlaceholder.text = @"Encantado de conocerte";
            break;
        case 4:
            ESBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            ESBasicTXTPlaceholder.text = @"No";
            break;
        case 5:
            ESBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            ESBasicTXTPlaceholder.text = @"Gracias";
            break;
        case 6:
            ESBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            ESBasicTXTPlaceholder.text = @"Sí";
            break;
        default:
            break;
    }
}

- (IBAction)ESBasicDidYouKnow:(id)sender {
    spanishDidYouKnow *ESDidYouKnow = [[spanishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESDidYouKnow animated:NO completion:NULL];
}

- (IBAction)ESBasicQuiz:(id)sender {
    spanishQuiz *ESQuiz = [[spanishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESQuiz animated:NO completion:NULL];
}

- (IBAction)ESPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([ESBasicTXTPlaceholder.text isEqual: @"Adiós"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeSpanish", CFSTR ("mp3"), NULL);
    }
    else if ([ESBasicTXTPlaceholder.text isEqual: @"Hola"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiSpanish", CFSTR ("mp3"), NULL);
    }
    else if ([ESBasicTXTPlaceholder.text isEqual: @"Me llamo"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsSpanish", CFSTR ("mp3"), NULL);
    }
    else if ([ESBasicTXTPlaceholder.text isEqual: @"Encantado de conocerte"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouSpanish", CFSTR ("mp3"), NULL);
    }
    else if ([ESBasicTXTPlaceholder.text isEqual: @"No"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noSpanish", CFSTR ("mp3"), NULL);
    }
    else if ([ESBasicTXTPlaceholder.text isEqual: @"Gracias"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouSpanish", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesSpanish", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end
