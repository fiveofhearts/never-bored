//
//  turkishView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "turkishView.h"
#import "turkishDidYouKnow.h"
#import "turkishBasic.h"
#import "turkishQuiz.h"

@interface turkishView ()

@end

@implementation turkishView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTurkish:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomTurkish:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        turkishDidYouKnow *TRDidYouKnow = [[turkishDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:TRDidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        turkishBasic *TRBasic = [[turkishBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:TRBasic animated:NO completion:NULL];
    }
    else {
        turkishQuiz *TRQuiz = [[turkishQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:TRQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchTRDidYouKnow:(id)sender {
    turkishDidYouKnow *TRDidYouKnow = [[turkishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRDidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchTRBasic:(id)sender {
    turkishBasic *TRBasic = [[turkishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRBasic animated:NO completion:NULL];
}

- (IBAction)switchTRQuiz:(id)sender {
    turkishQuiz *TRQuiz = [[turkishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRQuiz animated:NO completion:NULL];
}
@end
