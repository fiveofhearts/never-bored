//
//  watchView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface watchView : UIViewController {
    __weak IBOutlet UILabel *watchTut1;
    __weak IBOutlet UIButton *watchTut2;
    __weak IBOutlet UILabel *watchTut3;
    __weak IBOutlet UILabel *watchTut4;
    __weak IBOutlet UILabel *watchTut5;
}
- (IBAction)doneWatch:(id)sender;
- (IBAction)randomWatch:(id)sender;
@property (nonatomic, strong) MarqueeLabel *watchTitle;
@property (weak, nonatomic) IBOutlet UIWebView *watchPlaceholder;
@end
