//
//  germanView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "germanView.h"
#import "germanDidYouKnow.h"
#import "germanBasic.h"
#import "germanQuiz.h"

@interface germanView ()

@end

@implementation germanView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneGerman:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomGerman:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        germanDidYouKnow *DEDidYouKnow = [[germanDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:DEDidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        germanBasic *DEBasic = [[germanBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:DEBasic animated:NO completion:NULL];
    }
    else {
        germanQuiz *DEQuiz = [[germanQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:DEQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchDEDidYouKnow:(id)sender {
    germanDidYouKnow *DEDidYouKnow = [[germanDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEDidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchDEBasic:(id)sender {
    germanBasic *DEBasic = [[germanBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEBasic animated:NO completion:NULL];
}

- (IBAction)switchDEQuiz:(id)sender {
    germanQuiz *DEQuiz = [[germanQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEQuiz animated:NO completion:NULL];
}
@end

