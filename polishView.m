//
//  polishView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "polishView.h"
#import "polishDidYouKnow.h"
#import "polishBasic.h"
#import "polishQuiz.h"

@interface polishView ()

@end

@implementation polishView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)donePolish:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomPolish:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        polishDidYouKnow *PLDidYouKnow = [[polishDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:PLDidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        polishBasic *PLBasic = [[polishBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:PLBasic animated:NO completion:NULL];
    }
    else {
        polishQuiz *PLQuiz = [[polishQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:PLQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchPLDidYouKnow:(id)sender {
    polishDidYouKnow *PLDidYouKnow = [[polishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLDidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchPLBasic:(id)sender {
    polishBasic *PLBasic = [[polishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLBasic animated:NO completion:NULL];
}

- (IBAction)switchPLQuiz:(id)sender {
    polishQuiz *PLQuiz = [[polishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLQuiz animated:NO completion:NULL];
}
@end

