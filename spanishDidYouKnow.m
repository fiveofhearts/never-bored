//
//  spanishDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "spanishDidYouKnow.h"
#import "spanishBasic.h"
#import "spanishQuiz.h"

@interface spanishDidYouKnow ()

@end

@implementation spanishDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneESDidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomESDidYouKNow:(id)sender {
    ESDidYouKnowTut1.hidden = YES;
    ESDidYouKnowTut2.hidden = YES;
    ESDidYouKnowTut3.hidden = YES;
    ESDidYouKnowTut4.hidden = YES;
    ESDidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            ESDidYouKnowPlaceholder.text = @"Fact #1 \n \nThe name Spain diverged from the word Ispania, which means the land of rabbits.";
            break;
        case 1:
            ESDidYouKnowPlaceholder.text = @"Fact #2 \n \nThere is no tooth fairy in Spain but rather a tooth mouse called Ratoncito Perez.";
            break;
        case 2:
            ESDidYouKnowPlaceholder.text = @"Fact #3 \n \nSpain has over 8000 km of beaches.";
            break;
        case 3:
            ESDidYouKnowPlaceholder.text = @"Fact #4 \n \nThe Spanish national anthem is called La Marcha Real (The Royal March).";
            break;
        case 4:
            ESDidYouKnowPlaceholder.text = @"Fact #5 \n \nThe average life expectancy in Spain is 79.";
            break;
        case 5:
            ESDidYouKnowPlaceholder.text = @"Fact #6 \n \nSpain is 5 times bigger than the UK with only two thirds of its population.";
            break;
        case 6:
            ESDidYouKnowPlaceholder.text = @"Fact #7 \n \nThe two main newspapers in Spain are El País (The Country) and El Mundo (The World).";
            break;
        default:
            break;
    }
}

- (IBAction)ESDidYouKnowBasic:(id)sender {
    spanishBasic *ESBasic = [[spanishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESBasic animated:NO completion:NULL];
}

- (IBAction)ESDidYouKnowQuiz:(id)sender {
    spanishQuiz *ESQuiz = [[spanishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESQuiz animated:NO completion:NULL];
}
@end