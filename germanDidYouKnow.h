//
//  germanDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface germanDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *DEDidYouKnowPlaceholder;
    __weak IBOutlet UILabel *DEDidYouKnowTut1;
    __weak IBOutlet UIButton *DEDidYouKnowTut2;
    __weak IBOutlet UILabel *DEDidYouKnowTut3;
    __weak IBOutlet UILabel *DEDidYouKnowTut4;
    
}
- (IBAction)doneDEDidYouKnow:(id)sender;
- (IBAction)randomDEDidYouKNow:(id)sender;
- (IBAction)DEDidYouKnowBasic:(id)sender;
- (IBAction)DEDidYouKnowQuiz:(id)sender;
@end
