//
//  ticTacToeView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/10/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "ticTacToeView.h"

@interface ticTacToeView ()

@end

@implementation ticTacToeView
@synthesize whoseTurn, resetButton, board, s1, s2, s3, s4, s5, s6, s7, s8, s9;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) updatePLayerInfo
{
    if (playerToken == 1) {
        playerToken = 2;
        whoseTurn.text = @"O to move";
    }
    else if(playerToken == 2) {
        playerToken = 1;
        whoseTurn.text = @"X to move";
    }
    if ([self checkForWin])
    {
        if ([whoseTurn.text isEqual: @"O to move"])
        {
            UIAlertView *someonewon = [[UIAlertView alloc] initWithTitle:@"X wins!" message:@"Better luck next time, O!" delegate:self cancelButtonTitle:@"Restart" otherButtonTitles: nil];
            [someonewon show];
        }
        else
        {
            UIAlertView *someonewon = [[UIAlertView alloc] initWithTitle:@"O wins!" message:@"Better luck next time, X!" delegate:self cancelButtonTitle:@"Restart" otherButtonTitles: nil];
            [someonewon show];
        }
        [self resetBoard];
    }
}

-(void) resetBoard
{
    s1.image = NULL;
    s2.image = NULL;
    s3.image = NULL;
    s4.image = NULL;
    s5.image = NULL;
    s6.image = NULL;
    s7.image = NULL;
    s8.image = NULL;
    s9.image = NULL;
    playerToken= 1;
    whoseTurn.text = @"X goes first";
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if(CGRectContainsPoint([s1 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s1.image = xIMG; }
        if(playerToken==2){ s1.image = oIMG; } }
    if(CGRectContainsPoint([s2 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s2.image = xIMG; }
        if(playerToken==2){ s2.image = oIMG; } }
    if(CGRectContainsPoint([s3 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s3.image = xIMG; }
        if(playerToken==2){ s3.image = oIMG; } }
    if(CGRectContainsPoint([s4 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s4.image = xIMG; }
        if(playerToken==2){ s4.image = oIMG; } }
    if(CGRectContainsPoint([s5 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s5.image = xIMG; }
        if(playerToken==2){ s5.image = oIMG; } }
    if(CGRectContainsPoint([s6 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s6.image = xIMG; }
        if(playerToken==2){ s6.image = oIMG; } }
    if(CGRectContainsPoint([s7 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s7.image = xIMG; }
        if(playerToken==2){ s7.image = oIMG; } }
    if(CGRectContainsPoint([s8 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s8.image = xIMG; }
        if(playerToken==2){ s8.image = oIMG; } }
    if(CGRectContainsPoint([s9 frame], [touch locationInView:self.view])){
        if(playerToken==1){ s9.image = xIMG; }
        if(playerToken==2){ s9.image = oIMG; } }
    [self updatePLayerInfo]; }

-(BOOL) checkForWin
{
    if((s1.image == s2.image) & (s2.image == s3.image) & (s1.image != NULL))
    {
        return YES;
    }
    if((s4.image == s5.image) & (s5.image == s6.image) & (s4.image != NULL))
    {
        return YES;
    }
    if((s7.image == s8.image) & (s8.image == s9.image) & (s7.image != NULL))
    {
        return YES;
    }
    if((s1.image == s4.image) & (s4.image == s7.image) & (s1.image != NULL))
    {
        return YES;
    }
    if((s2.image == s5.image) & (s5.image == s8.image) & (s2.image != NULL))
    {
        return YES;
    }
    if((s3.image == s6.image) & (s6.image == s9.image) & (s3.image != NULL))
    {
        return YES;
    }
    if((s1.image == s5.image) & (s5.image == s9.image) & (s1.image != NULL))
    {
        return YES;
    }
    if((s3.image == s5.image) & (s5.image == s7.image) & (s3.image != NULL))
    {
        return YES;
    }
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    oIMG = [UIImage imageNamed:@"ticTacToeO.png"];
    xIMG = [UIImage imageNamed:@"ticTacToeX.png"];
    playerToken = 1;
    whoseTurn.text = @"X goes first";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTicTacToe:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)buttonReset:(id)sender {
    [self resetBoard];
}
@end
