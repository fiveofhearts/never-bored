//
//  ukrainianDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ukrainianDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *UADidYouKnowPlaceholder;
    __weak IBOutlet UILabel *UADidYouKnowTut1;
    __weak IBOutlet UIButton *UADidYouKnowTut2;
    __weak IBOutlet UILabel *UADidYouKnowTut3;
    __weak IBOutlet UILabel *UADidYouKnowTut4;
    
}
- (IBAction)doneUADidYouKnow:(id)sender;
- (IBAction)randomUADidYouKNow:(id)sender;
- (IBAction)UADidYouKnowBasic:(id)sender;
- (IBAction)UADidYouKnowQuiz:(id)sender;
@end
