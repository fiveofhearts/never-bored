//
//  germanDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "germanDidYouKnow.h"
#import "germanBasic.h"
#import "germanQuiz.h"

@interface germanDidYouKnow ()

@end

@implementation germanDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneDEDidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomDEDidYouKNow:(id)sender {
    DEDidYouKnowTut1.hidden = YES;
    DEDidYouKnowTut2.hidden = YES;
    DEDidYouKnowTut3.hidden = YES;
    DEDidYouKnowTut4.hidden = YES;
    DEDidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            DEDidYouKnowPlaceholder.text = @"Fact #1 \n \nNearly half of all Germans are fluent in English, but only 3 percent of Germans speak French fluently.";
            break;
        case 1:
            DEDidYouKnowPlaceholder.text = @"Fact #2 \n \nGermans value punctuality. In Germany, if guests are invited for dinner at 8 p.m., they will arrive at 8 p.m., whereas in Canada it is considered polite to arrive a few minutes after the hour.";
            break;
        case 2:
            DEDidYouKnowPlaceholder.text = @"Fact #3 \n \nGermans wear their wedding rings on their right hands, not on the left.";
            break;
        case 3:
            DEDidYouKnowPlaceholder.text = @"Fact #4 \n \nChimney sweeps can still be seen in Germany, dressed in the traditional black suit and black top hat.";
            break;
        case 4:
            DEDidYouKnowPlaceholder.text = @"Fact #5 \n \nOnly about 2 percent of Germans do not own cell phones.";
            break;
        case 5:
            DEDidYouKnowPlaceholder.text = @"Fact #6 \n \nGummy Bears were invented by a German, Hans Riegel.";
            break;
        case 6:
            DEDidYouKnowPlaceholder.text = @"Fact #7 \n \nGermany's Zoo Berlin has the largest number of species in the world.";
            break;
        default:
            break;
    }
}

- (IBAction)DEDidYouKnowBasic:(id)sender {
    germanBasic *DEBasic = [[germanBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEBasic animated:NO completion:NULL];
}

- (IBAction)DEDidYouKnowQuiz:(id)sender {
    germanQuiz *DEQuiz = [[germanQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEQuiz animated:NO completion:NULL];
}
@end
