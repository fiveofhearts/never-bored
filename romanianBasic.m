//
//  romanianBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "romanianBasic.h"
#import "romanianDidYouKnow.h"
#import "romanianQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface romanianBasic ()

@end

@implementation romanianBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneROBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomROBasic:(id)sender {
    ROBasicTut1.hidden = YES;
    ROBasicTut2.hidden = YES;
    ROBasicTut3.hidden = YES;
    ROBasicTut4.hidden = YES;
    ROBasicIMGPlaceholder.hidden = NO;
    ROBasicPlayButton.hidden = NO;
    ROBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            ROBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            ROBasicTXTPlaceholder.text = @"La revedere";
            break;
        case 1:
            ROBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            ROBasicTXTPlaceholder.text = @"Salut";
            break;
        case 2:
            ROBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            ROBasicTXTPlaceholder.text = @"Mă numesc";
            break;
        case 3:
            ROBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            ROBasicTXTPlaceholder.text = @"Îmi pare bine";
            break;
        case 4:
            ROBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            ROBasicTXTPlaceholder.text = @"Nu";
            break;
        case 5:
            ROBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            ROBasicTXTPlaceholder.text = @"Mulţumesc";
            break;
        case 6:
            ROBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            ROBasicTXTPlaceholder.text = @"Da";
            break;
        default:
            break;
    }
}

- (IBAction)ROBasicDidYouKnow:(id)sender {
    romanianDidYouKnow *RODidYouKnow = [[romanianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RODidYouKnow animated:NO completion:NULL];
}

- (IBAction)ROBasicQuiz:(id)sender {
    romanianQuiz *ROQuiz = [[romanianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ROQuiz animated:NO completion:NULL];
}

- (IBAction)ROPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([ROBasicTXTPlaceholder.text isEqual: @"La revedere"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeRomanian", CFSTR ("mp3"), NULL);
    }
    else if ([ROBasicTXTPlaceholder.text isEqual: @"Salut"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiRomanian", CFSTR ("mp3"), NULL);
    }
    else if ([ROBasicTXTPlaceholder.text isEqual: @"Mă numesc"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsRomanian", CFSTR ("mp3"), NULL);
    }
    else if ([ROBasicTXTPlaceholder.text isEqual: @"Îmi pare bine"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouRomanian", CFSTR ("mp3"), NULL);
    }
    else if ([ROBasicTXTPlaceholder.text isEqual: @"Nu"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noRomanian", CFSTR ("mp3"), NULL);
    }
    else if ([ROBasicTXTPlaceholder.text isEqual: @"Mulţumesc"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouRomanian", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesRomanian", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end