//
//  ticTacToeView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/10/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ticTacToeView : UIViewController {
    IBOutlet UIImage *oIMG;
    IBOutlet UIImage *xIMG;
    __weak IBOutlet UIImageView *s1;
    __weak IBOutlet UIImageView *s2;
    __weak IBOutlet UIImageView *s3;
    __weak IBOutlet UIImageView *s4;
    __weak IBOutlet UIImageView *s5;
    __weak IBOutlet UIImageView *s6;
    __weak IBOutlet UIImageView *s7;
    __weak IBOutlet UIImageView *s8;
    __weak IBOutlet UIImageView *s9;
    __weak IBOutlet UILabel *whoseTurn;
    __weak IBOutlet UIImageView *board;
    __weak IBOutlet UIButton *resetButton;
    NSInteger playerToken;
}
- (IBAction)doneTicTacToe:(id)sender;
- (IBAction)buttonReset:(id)sender;
- (void) updatePLayerInfo;
- (void) resetBoard;
- (BOOL) checkForWin;
@property (weak, nonatomic) IBOutlet UIImageView *board;
@property (weak, nonatomic) IBOutlet UILabel *whoseTurn;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UIImageView *s1;
@property (weak, nonatomic) IBOutlet UIImageView *s2;
@property (weak, nonatomic) IBOutlet UIImageView *s3;
@property (weak, nonatomic) IBOutlet UIImageView *s4;
@property (weak, nonatomic) IBOutlet UIImageView *s5;
@property (weak, nonatomic) IBOutlet UIImageView *s6;
@property (weak, nonatomic) IBOutlet UIImageView *s7;
@property (weak, nonatomic) IBOutlet UIImageView *s8;
@property (weak, nonatomic) IBOutlet UIImageView *s9;

@end
