//
//  ukrainianView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ukrainianView : UIViewController {
}
- (IBAction)doneUkrainian:(id)sender;
- (IBAction)randomUkrainian:(id)sender;
- (IBAction)switchUADidYouKnow:(id)sender;
- (IBAction)switchUABasic:(id)sender;
- (IBAction)switchUAQuiz:(id)sender;
@end
