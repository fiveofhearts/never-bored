//
//  playView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface playView : UIViewController {
}
- (IBAction)donePlay:(id)sender;
- (IBAction)randomPlay:(id)sender;
- (IBAction)switchPong:(id)sender;
- (IBAction)switchFindTheBee:(id)sender;
- (IBAction)switchTicTacToe:(id)sender;

@end
