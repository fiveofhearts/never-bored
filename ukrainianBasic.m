//
//  ukrainianBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "ukrainianBasic.h"
#import "ukrainianDidYouKnow.h"
#import "ukrainianQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface ukrainianBasic ()

@end

@implementation ukrainianBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneUABasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomUABasic:(id)sender {
    UABasicTut1.hidden = YES;
    UABasicTut2.hidden = YES;
    UABasicTut3.hidden = YES;
    UABasicTut4.hidden = YES;
    UABasicIMGPlaceholder.hidden = NO;
    UABasicPlayButton.hidden = NO;
    UABasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            UABasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            UABasicTXTPlaceholder.text = @"До побачення";
            break;
        case 1:
            UABasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            UABasicTXTPlaceholder.text = @"Добрий день";
            break;
        case 2:
            UABasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            UABasicTXTPlaceholder.text = @"Мене звати";
            break;
        case 3:
            UABasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            UABasicTXTPlaceholder.text = @"Радий/а з вами познайомитися";
            break;
        case 4:
            UABasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            UABasicTXTPlaceholder.text = @"Ni";
            break;
        case 5:
            UABasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            UABasicTXTPlaceholder.text = @"Дякую";
            break;
        case 6:
            UABasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            UABasicTXTPlaceholder.text = @"Tak";
            break;
        default:
            break;
    }
}

- (IBAction)UABasicDidYouKnow:(id)sender {
    ukrainianDidYouKnow *UADidYouKnow = [[ukrainianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UADidYouKnow animated:NO completion:NULL];
}

- (IBAction)UABasicQuiz:(id)sender {
    ukrainianQuiz *UAQuiz = [[ukrainianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UAQuiz animated:NO completion:NULL];
}

- (IBAction)UAPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([UABasicTXTPlaceholder.text isEqual: @"До побачення"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeUkrainean", CFSTR ("mp3"), NULL);
    }
    else if ([UABasicTXTPlaceholder.text isEqual: @"Добрий день"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiUkranian", CFSTR ("mp3"), NULL);
    }
    else if ([UABasicTXTPlaceholder.text isEqual: @"Мене звати"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsUkrainean", CFSTR ("mp3"), NULL);
    }
    else if ([UABasicTXTPlaceholder.text isEqual: @"Радий/а з вами познайомитися"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouUkrainean", CFSTR ("mp3"), NULL);
    }
    else if ([UABasicTXTPlaceholder.text isEqual: @"Ni"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noUkrainean", CFSTR ("mp3"), NULL);
    }
    else if ([UABasicTXTPlaceholder.text isEqual: @"Дякую"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouUkrainean", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesUkranian", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end
