//
//  russianView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "russianView.h"
#import "russianDidYouKnow.h"
#import "russianBasic.h"
#import "russianQuiz.h"

@interface russianView ()

@end

@implementation russianView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneRussian:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomRussian:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        russianDidYouKnow *RUDidYouKnow = [[russianDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:RUDidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        russianBasic *RUBasic = [[russianBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:RUBasic animated:NO completion:NULL];
    }
    else {
        russianQuiz *RUQuiz = [[russianQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:RUQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchRUDidYouKnow:(id)sender {
    russianDidYouKnow *RUDidYouKnow = [[russianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUDidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchRUBasic:(id)sender {
    russianBasic *RUBasic = [[russianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUBasic animated:NO completion:NULL];
}

- (IBAction)switchRUQuiz:(id)sender {
    russianQuiz *RUQuiz = [[russianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUQuiz animated:NO completion:NULL];
}
@end
