//
//  turkishQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "turkishQuiz.h"
#import "turkishDidYouKnow.h"
#import "turkishBasic.h"

@interface turkishQuiz ()

@end

@implementation turkishQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTRQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomTRQuiz:(id)sender {
    TRQuizTut1.hidden = YES;
    TRQuizTut2.hidden = YES;
    TRQuizTut3.hidden = YES;
    TRQuizTut4.hidden = YES;
    TRQuizQuestion.hidden = NO;
    TRQuizButtonA.hidden = NO;
    TRQuizButtonB.hidden = NO;
    TRQuizButtonC.hidden = NO;
    TRQuizButtonD.hidden = NO;
    TRQuizAnswer1Label.hidden = NO;
    TRQuizAnswer2Label.hidden = NO;
    TRQuizAnswer3Label.hidden = NO;
    TRQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            TRQuizQuestion.text = @"Istanbul is the only city in the world that straddles two continents:";
            TRQuizAnswer1Label.text = @"Asia and Europe";
            TRQuizAnswer2Label.text = @"Asia and Africa";
            TRQuizAnswer3Label.text = @"Africa and Europe";
            TRQuizAnswer4Label.text = @"Europe and Australia";
            TRQuizResult.text = @"";
            break;
        case 1:
            TRQuizQuestion.text = @"Smallpox vaccination was introduced to England and Europe from Turkey by Lady Montagu in early ...";
            TRQuizAnswer1Label.text = @"17th century";
            TRQuizAnswer2Label.text = @"18th century";
            TRQuizAnswer3Label.text = @"19th century";
            TRQuizAnswer4Label.text = @"20th century";
            TRQuizResult.text = @"";
            break;
        case 2:
            TRQuizQuestion.text = @"One of the biggest theatres of antiquity seating ... is Aspendos.";
            TRQuizAnswer1Label.text = @"12,000";
            TRQuizAnswer2Label.text = @"13,000";
            TRQuizAnswer3Label.text = @"15,000";
            TRQuizAnswer4Label.text = @"14,000";
            TRQuizResult.text = @"";
            break;
        case 3:
            TRQuizQuestion.text = @"The capital of Turkey is ...";
            TRQuizAnswer1Label.text = @"Istanbul";
            TRQuizAnswer2Label.text = @"Izmir";
            TRQuizAnswer3Label.text = @"Bursa";
            TRQuizAnswer4Label.text = @"Ankara";
            TRQuizResult.text = @"";
            break;
        case 4:
            TRQuizQuestion.text = @"Benim adım means ... in Turkish.";
            TRQuizAnswer1Label.text = @"Hello";
            TRQuizAnswer2Label.text = @"Goodbye";
            TRQuizAnswer3Label.text = @"Yes";
            TRQuizAnswer4Label.text = @"My name is";
            TRQuizResult.text = @"";
            break;
        case 5:
            TRQuizQuestion.text = @"Merhaba means ... in Turkish.";
            TRQuizAnswer1Label.text = @"Goodbye";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Hello";
            TRQuizAnswer4Label.text = @"No";
            TRQuizResult.text = @"";
            break;
        case 6:
            TRQuizQuestion.text = @"Teşekkür ederim means ... in Turkish.";
            TRQuizAnswer1Label.text = @"Yes";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Goodbye";
            TRQuizAnswer4Label.text = @"Hello";
            TRQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)TRQuizDidYouKnow:(id)sender {
    turkishDidYouKnow *TRDidYouKnow = [[turkishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRDidYouKnow animated:NO completion:NULL];
}

- (IBAction)TRQuizBasic:(id)sender {
    turkishBasic *TRBasic = [[turkishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRBasic animated:NO completion:NULL];
}

- (IBAction)TRQuizAnswer1:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Istanbul is the only city in the world that straddles two continents:"]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Smallpox vaccination was introduced to England and Europe from Turkey by Lady Montagu in early ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"One of the biggest and best preserved theatres of antiquity seating ... is Aspendos."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The capital of Turkey is ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Benim adım means ... in Turkish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Merhaba means ... in Turkish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer2:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Istanbul is the only city in the world that straddles two continents:"]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Smallpox vaccination was introduced to England and Europe from Turkey by Lady Montagu in early ..."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"One of the biggest and best preserved theatres of antiquity seating ... is Aspendos."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The capital of Turkey is ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Benim adım means ... in Turkish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Merhaba means ... in Turkish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)TRQuizAnswer3:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Istanbul is the only city in the world that straddles two continents:"]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Smallpox vaccination was introduced to England and Europe from Turkey by Lady Montagu in early ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"One of the biggest and best preserved theatres of antiquity seating ... is Aspendos."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"The capital of Turkey is ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Benim adım means ... in Turkish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Merhaba means ... in Turkish."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer4:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Istanbul is the only city in the world that straddles two continents:"]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Smallpox vaccination was introduced to England and Europe from Turkey by Lady Montagu in early ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"One of the biggest and best preserved theatres of antiquity seating ... is Aspendos."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The capital of Turkey is ..."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Benim adım means ... in Turkish."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Merhaba means ... in Turkish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
}
@end
