//
//  findTheBeeView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/13/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface findTheBeeView : UIViewController {
    NSUInteger hiddenLocation;
    NSTimer *clock;
    NSUInteger elapsed_seconds;
}
@property (nonatomic, assign) IBOutlet UIButton *startButton;
@property (nonatomic, assign) IBOutlet UITextField *timeRemaining;
- (IBAction)doneFindTheBee:(id)sender;
- (IBAction)startGame:(id)sender;
- (void)guess:(id)sender;
- (void)tick:(NSTimer *)timer;
- (void)resetGame;
@end

