//
//  learnView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface learnView : UIViewController {
}
- (IBAction)doneLearn:(id)sender;
- (IBAction)randomLearn:(id)sender;
@end
