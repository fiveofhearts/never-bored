//
//  frenchBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface frenchBasic : UIViewController {
    __weak IBOutlet UIImageView *FRBasicIMGPlaceholder;
    __weak IBOutlet UILabel *FRBasicTXTPlaceholder;
    __weak IBOutlet UIButton *FRBasicPlayButton;
    __weak IBOutlet UILabel *FRBasicTut1;
    __weak IBOutlet UIButton *FRBasicTut2;
    __weak IBOutlet UILabel *FRBasicTut3;
    __weak IBOutlet UILabel *FRBasicTut4;
}
- (IBAction)doneFRBasic:(id)sender;
- (IBAction)randomFRBasic:(id)sender;
- (IBAction)FRBasicDidYouKnow:(id)sender;
- (IBAction)FRBasicQuiz:(id)sender;
- (IBAction)FRPlay:(id)sender;

@end
