//
//  danishView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/2/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface danishView : UIViewController {
}
- (IBAction)doneDanish:(id)sender;
- (IBAction)randomDanish:(id)sender;
- (IBAction)switchDKDidYouKnow:(id)sender;
- (IBAction)switchDKBasic:(id)sender;
- (IBAction)switchDKQuiz:(id)sender;
@end
