//
//  frenchView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "frenchView.h"
#import "frenchDidYouKnow.h"
#import "frenchBasic.h"
#import "frenchQuiz.h"

@interface frenchView ()

@end

@implementation frenchView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneFrench:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomFrench:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        frenchDidYouKnow *FRDidYouKnow = [[frenchDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:FRDidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        frenchBasic *FRBasic = [[frenchBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:FRBasic animated:NO completion:NULL];
    }
    else {
        frenchQuiz *FRQuiz = [[frenchQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:FRQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchFRDidYouKnow:(id)sender {
    frenchDidYouKnow *FRDidYouKnow = [[frenchDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRDidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchFRBasic:(id)sender {
    frenchBasic *FRBasic = [[frenchBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRBasic animated:NO completion:NULL];
}

- (IBAction)switchFRQuiz:(id)sender {
    frenchQuiz *FRQuiz = [[frenchQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRQuiz animated:NO completion:NULL];
}
@end
