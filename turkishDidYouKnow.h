//
//  turkishDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface turkishDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *TRDidYouKnowPlaceholder;
    __weak IBOutlet UILabel *TRDidYouKnowTut1;
    __weak IBOutlet UIButton *TRDidYouKnowTut2;
    __weak IBOutlet UILabel *TRDidYouKnowTut3;
    __weak IBOutlet UILabel *TRDidYouKnowTut4;
    
}
- (IBAction)doneTRDidYouKnow:(id)sender;
- (IBAction)randomTRDidYouKNow:(id)sender;
- (IBAction)TRDidYouKnowBasic:(id)sender;
- (IBAction)TRDidYouKnowQuiz:(id)sender;
@end

