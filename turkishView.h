//
//  turkishView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface turkishView : UIViewController {
}
- (IBAction)doneTurkish:(id)sender;
- (IBAction)randomTurkish:(id)sender;
- (IBAction)switchTRDidYouKnow:(id)sender;
- (IBAction)switchTRBasic:(id)sender;
- (IBAction)switchTRQuiz:(id)sender;
@end
