//
//  findTheBeeView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/13/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "findTheBeeView.h"

@interface findTheBeeView ()

@end

@implementation findTheBeeView
@synthesize startButton, timeRemaining;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	for (int i=1001; i < 1010; ++i) {
        UIButton *b = (UIButton *)[self.view viewWithTag:i];
        [b addTarget:self action:@selector(guess:) forControlEvents:UIControlEventTouchUpInside];
    }
	[self resetGame];
	[self becomeFirstResponder];
    // Do any additional setup after loading the view from its nib.
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
	NSLog(@"motion %@", event);
	if (motion == UIEventSubtypeMotionShake) {
		[self startGame:nil];
	}
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
	NSLog(@"motion began %@", event);
	if (motion == UIEventSubtypeMotionShake) {
		[self startGame:nil];
	}
}

- (void) viewDidUnload {
	[super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[self becomeFirstResponder];
}

- (void)resetGame {
	CABasicAnimation *trans = [CABasicAnimation animation];
	trans.keyPath = @"transform.scale";
	trans.repeatCount = HUGE_VALF;
	trans.duration = 0.5;
	trans.autoreverses = YES;
	trans.removedOnCompletion = NO;
	trans.fillMode = kCAFillModeForwards;
	trans.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	trans.fromValue = [NSNumber numberWithFloat:0.9];
	trans.toValue = [NSNumber numberWithFloat:1.1];
	[self.startButton.titleLabel.layer addAnimation:trans forKey:@"pulse"];
	for (int i=1001; i < 1010; ++i) [(UIButton *)[self.view viewWithTag:i] setEnabled:NO];
	elapsed_seconds = 0;
}

- (IBAction)startGame:(id)sender {
	for (int i=1001; i < 1010; ++i) {
		UIButton *b = (UIButton *)[self.view viewWithTag:i];
		[b setImage:nil forState:UIControlStateNormal];
		[b setTitle:@"?" forState:UIControlStateNormal];
		b.enabled = YES;
	}
	[self.startButton.titleLabel.layer removeAllAnimations];
	NSUInteger isCorrect = 0;
	SecRandomCopyBytes(kSecRandomDefault, sizeof(NSUInteger), (void *)&isCorrect);
	hiddenLocation = isCorrect % 9;
	elapsed_seconds = 0;
	[timeRemaining setText:@"00:00:00"];
	if (clock) {
		[clock invalidate];
	}
	clock = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tick:) userInfo:nil repeats:YES];
}

- (BOOL)canBecomeFirstResponder { return YES; }

- (void)guess:(id)sender
{
	UIButton *guessed = (UIButton *)sender;
	guessed.enabled = NO;
	CATransition *trans = [[CATransition alloc] init];
	trans.duration = 0.25;
	trans.type = kCATransitionFade;
	[guessed.layer addAnimation:trans forKey:@"Fade"];
	[CATransaction begin];
	if (guessed.tag - 1001 == hiddenLocation)
    {
		[guessed setTitle:@"" forState:UIControlStateNormal];
		[guessed setImage:[UIImage imageNamed:@"bee"] forState:UIControlStateNormal];
		[clock invalidate];
		clock = nil;
		[self resetGame];
        UIAlertView *someonewon = [[UIAlertView alloc] initWithTitle:@"Great job!" message:@"You found the bee." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [someonewon show];
	} else {
        [guessed setTitle:@"" forState:UIControlStateNormal];
        [guessed setImage:[UIImage imageNamed:@"honey"] forState:UIControlStateNormal];
	}
	[CATransaction commit];
}

- (void)tick:(NSTimer *)timer
{
	++elapsed_seconds;
	[timeRemaining setText:[NSString stringWithFormat:@"%02d:%02d:%02d",
							elapsed_seconds / 3600, (elapsed_seconds % 3600) / 60, elapsed_seconds % 60]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneFindTheBee:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
