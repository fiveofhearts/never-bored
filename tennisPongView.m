//
//  tennisPongView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/10/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "tennisPongView.h"
#define kGameStateRunning 1
#define kGameStatePaused 2

#define kBallSpeedX 3
#define kBallSpeedY 4

#define kComputerMoveSpeed 3.25
#define kScoreToWin 15

@interface tennisPongView ()

@end

@implementation tennisPongView

@synthesize playerPaddle, computerPaddle, ball, playerScoreText, computerScoreText, winOrLoseLabel, ballVelocity, gameState;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:touch.view];
    playerPaddle.center = CGPointMake(playerPaddle.center.x, location.y);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesBegan:touches withEvent:event];
}

- (void)gameLoop
{
    if (gameState == kGameStateRunning) {
        playerScoreText.hidden = YES;
        computerScoreText.hidden = YES;
        winOrLoseLabel.hidden = YES;
        ball.center = CGPointMake(ball.center.x + ballVelocity.x, ball.center.y + ballVelocity.y);
        if (ball.center.x > self.view.bounds.size.height || ball.center.x < 0) {
            ballVelocity.x = -ballVelocity.x;
        }
        if (ball.center.y > self.view.bounds.size.width || ball.center.y < 0) {
            ballVelocity.y = -ballVelocity.y;
        }
        if (CGRectIntersectsRect(ball.frame, playerPaddle.frame)) {
            CGRect frame = ball.frame;
            frame.origin.x = playerPaddle.frame.origin.x + frame.size.height;
            ball.frame = frame;
            ballVelocity.x = -ballVelocity.x;
        }
        if (CGRectIntersectsRect(ball.frame, computerPaddle.frame)) {
            CGRect frame = ball.frame;
            frame.origin.x = CGRectGetMaxX(computerPaddle.frame);
            ball.frame = frame;
            ballVelocity.x = -ballVelocity.x;
        }
        if (ball.center.x <= self.view.center.x) {
            if (ball.center.y < computerPaddle.center.y) {
                CGPoint compLocation = CGPointMake(computerPaddle.center.x, computerPaddle.center.y + kComputerMoveSpeed);
                computerPaddle.center = compLocation;
            }
            if (ball.center.y > computerPaddle.center.y) {
                CGPoint compLocation = CGPointMake(computerPaddle.center.x, computerPaddle.center.y + kComputerMoveSpeed);
                computerPaddle.center = compLocation;
            }
        }
        if (ball.center.x <= 0) {
            playerScoreValue++;
            [self reset:(playerScoreValue >= kScoreToWin)];
        }
        if (ball.center.x > self.view.bounds.size.width) {
            computerScoreValue++;
            [self reset:(computerScoreValue >= kScoreToWin)];
        }
    }
}

- (void)reset:(BOOL)newGame
{
    self.gameState = kGameStatePaused;
    ball.center = CGPointMake(ball.center.x + ballVelocity.x , ball.center.y + ballVelocity.y);
    playerScoreText.hidden = NO;
    computerScoreText.hidden = NO;
    playerScoreText.text = [NSString stringWithFormat:@"%d",playerScoreValue];
    computerScoreText.text = [NSString stringWithFormat:@"%d",computerScoreValue];
    if (newGame) {
        winOrLoseLabel.hidden = NO;
        if (computerScoreValue > playerScoreValue) {
            winOrLoseLabel.text = @"Game Over!";
        } else {
            winOrLoseLabel.text = @"You Win!";
        }
        playerScoreValue = 0;
        computerScoreValue = 0;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.gameState = kGameStatePaused;
    ballVelocity = CGPointMake(kBallSpeedX, kBallSpeedY);
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(gameLoop) userInfo:nil repeats:YES];
    winOrLoseLabel.hidden = YES;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTennisPong:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
