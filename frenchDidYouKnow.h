//
//  frenchDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface frenchDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *FRDidYouKnowPlaceholder;
    __weak IBOutlet UILabel *FRDidYouKnowTut1;
    __weak IBOutlet UIButton *FRDidYouKnowTut2;
    __weak IBOutlet UILabel *FRDidYouKnowTut3;
    __weak IBOutlet UILabel *FRDidYouKnowTut4;
    
}
- (IBAction)doneFRDidYouKnow:(id)sender;
- (IBAction)randomFRDidYouKNow:(id)sender;
- (IBAction)FRDidYouKnowBasic:(id)sender;
- (IBAction)FRDidYouKnowQuiz:(id)sender;
@end
