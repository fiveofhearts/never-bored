//
//  polishBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface polishBasic : UIViewController {
    __weak IBOutlet UIImageView *PLBasicIMGPlaceholder;
    __weak IBOutlet UILabel *PLBasicTXTPlaceholder;
    __weak IBOutlet UIButton *PLBasicPlayButton;
    __weak IBOutlet UILabel *PLBasicTut1;
    __weak IBOutlet UIButton *PLBasicTut2;
    __weak IBOutlet UILabel *PLBasicTut3;
    __weak IBOutlet UILabel *PLBasicTut4;
}
- (IBAction)donePLBasic:(id)sender;
- (IBAction)randomPLBasic:(id)sender;
- (IBAction)PLBasicDidYouKnow:(id)sender;
- (IBAction)PLBasicQuiz:(id)sender;
- (IBAction)PLPlay:(id)sender;

@end
