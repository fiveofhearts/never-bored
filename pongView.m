//
//  pongView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/9/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "pongView.h"
@interface pongView ()

@end

@implementation pongView

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:touch.view];
    Paddle.center = CGPointMake(location.x, Paddle.center.y);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesBegan:touches withEvent:event];
}

- (void)CPU {
    Ball.center = CGPointMake(Ball.center.x + Xgain, Ball.center.y + Ygain);
    if (Ball.center.x < 20) {
        Xgain = abs(Xgain);
    }
    if (Ball.center.x > 300) {
        Xgain = -abs(Xgain);
    }
    if (Ball.center.y < 84) {
        Ygain = abs(Ygain);
    }
    if (Ball.center.y > 460 && Ball.center.y < 480) {
        NSString *highScore = [NSString stringWithFormat:@"Your score is: %@", pongScore.text];
        UIAlertView *someonewon = [[UIAlertView alloc] initWithTitle:@"Game Over!" message:highScore delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [someonewon show];
        score = 0;
    }
    if (CGRectIntersectsRect(Ball.frame, Paddle.frame)) {
        Ygain = -abs(Ygain);
        score = score + 1;
    }
    NSString *strScore = [NSString stringWithFormat:@"%d",score];
    pongScore.text = strScore;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(CPU) userInfo:nil repeats:YES];
    Xgain = 20;
    Ygain = 20;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)donePong:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    Xgain = 0;
    Ygain = 0;
}

- (IBAction)restartPong:(id)sender {
    Ball.center = CGPointMake(150 + Xgain, 72 + Ygain);
}
@end
