//
//  frenchQuiz.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface frenchQuiz : UIViewController {
    __weak IBOutlet UITextView *FRQuizQuestion;
    __weak IBOutlet UILabel *FRQuizAnswer1Label;
    __weak IBOutlet UILabel *FRQuizAnswer2Label;
    __weak IBOutlet UILabel *FRQuizAnswer3Label;
    __weak IBOutlet UILabel *FRQuizAnswer4Label;
    __weak IBOutlet UILabel *FRQuizResult;
    __weak IBOutlet UIButton *FRQuizButtonA;
    __weak IBOutlet UIButton *FRQuizButtonB;
    __weak IBOutlet UIButton *FRQuizButtonC;
    __weak IBOutlet UIButton *FRQuizButtonD;
    __weak IBOutlet UILabel *FRQuizTut1;
    __weak IBOutlet UIButton *FRQuizTut2;
    __weak IBOutlet UILabel *FRQuizTut3;
    __weak IBOutlet UILabel *FRQuizTut4;
}
- (IBAction)doneFRQuiz:(id)sender;
- (IBAction)randomFRQuiz:(id)sender;
- (IBAction)FRQuizDidYouKnow:(id)sender;
- (IBAction)FRQuizBasic:(id)sender;
- (IBAction)FRQuizAnswer1:(id)sender;
- (IBAction)FRQuizAnswer2:(id)sender;
- (IBAction)FRQuizAnswer3:(id)sender;
- (IBAction)FRQuizAnswer4:(id)sender;

@end
