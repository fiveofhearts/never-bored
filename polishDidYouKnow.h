//
//  polishDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface polishDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *PLDidYouKnowPlaceholder;
    __weak IBOutlet UILabel *PLDidYouKnowTut1;
    __weak IBOutlet UIButton *PLDidYouKnowTut2;
    __weak IBOutlet UILabel *PLDidYouKnowTut3;
    __weak IBOutlet UILabel *PLDidYouKnowTut4;
    
}
- (IBAction)donePLDidYouKnow:(id)sender;
- (IBAction)randomPLDidYouKNow:(id)sender;
- (IBAction)PLDidYouKnowBasic:(id)sender;
- (IBAction)PLDidYouKnowQuiz:(id)sender;
@end
