//
//  Never_BoredTests.m
//  Never BoredTests
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Never_BoredTests : XCTestCase

@end

@implementation Never_BoredTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
