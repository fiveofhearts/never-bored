//
//  pongView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/9/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pongView : UIViewController {
    IBOutlet UIImageView *Paddle;
    IBOutlet UIImageView *Ball;
    IBOutlet UILabel *pongScore;
    NSInteger Xgain;
    NSInteger Ygain;
    NSTimer *timer;
    int score;
}
- (void)CPU;
- (IBAction)donePong:(id)sender;
- (IBAction)restartPong:(id)sender;
@end
