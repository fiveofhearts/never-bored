//
//  dutchDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dutchDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *NLDidYouKnowPlaceholder;
    __weak IBOutlet UILabel *NLDidYouKnowTut1;
    __weak IBOutlet UIButton *NLDidYouKnowTut2;
    __weak IBOutlet UILabel *NLDidYouKnowTut3;
    __weak IBOutlet UILabel *NLDidYouKnowTut4;
    
}
- (IBAction)doneNLDidYouKnow:(id)sender;
- (IBAction)randomNLDidYouKNow:(id)sender;
- (IBAction)NLDidYouKnowBasic:(id)sender;
- (IBAction)NLDidYouKnowQuiz:(id)sender;
@end
