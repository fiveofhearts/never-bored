//
//  dutchBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dutchBasic : UIViewController {
    __weak IBOutlet UIImageView *NLBasicIMGPlaceholder;
    __weak IBOutlet UILabel *NLBasicTXTPlaceholder;
    __weak IBOutlet UIButton *NLBasicPlayButton;
    __weak IBOutlet UILabel *NLBasicTut1;
    __weak IBOutlet UIButton *NLBasicTut2;
    __weak IBOutlet UILabel *NLBasicTut3;
    __weak IBOutlet UILabel *NLBasicTut4;
}
- (IBAction)doneNLBasic:(id)sender;
- (IBAction)randomNLBasic:(id)sender;
- (IBAction)NLBasicDidYouKnow:(id)sender;
- (IBAction)NLBasicQuiz:(id)sender;
- (IBAction)NLPlay:(id)sender;

@end
