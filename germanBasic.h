//
//  germanBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface germanBasic : UIViewController {
    __weak IBOutlet UIImageView *DEBasicIMGPlaceholder;
    __weak IBOutlet UILabel *DEBasicTXTPlaceholder;
    __weak IBOutlet UIButton *DEBasicPlayButton;
    __weak IBOutlet UILabel *DEBasicTut1;
    __weak IBOutlet UIButton *DEBasicTut2;
    __weak IBOutlet UILabel *DEBasicTut3;
    __weak IBOutlet UILabel *DEBasicTut4;
}
- (IBAction)doneDEBasic:(id)sender;
- (IBAction)randomDEBasic:(id)sender;
- (IBAction)DEBasicDidYouKnow:(id)sender;
- (IBAction)DEBasicQuiz:(id)sender;
- (IBAction)DEPlay:(id)sender;

@end
