//
//  romanianDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "romanianDidYouKnow.h"
#import "romanianBasic.h"
#import "romanianQuiz.h"

@interface romanianDidYouKnow ()

@end

@implementation romanianDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneRODidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomRODidYouKNow:(id)sender {
    RODidYouKnowTut1.hidden = YES;
    RODidYouKnowTut2.hidden = YES;
    RODidYouKnowTut3.hidden = YES;
    RODidYouKnowTut4.hidden = YES;
    RODidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            RODidYouKnowPlaceholder.text = @"Fact #1 \n \nThe archetypical vampire Count Dracula, created by Bram Stoker, was inspired by the pitiless Romanian general Vlad Tepes, also known as Vlad the Impaler because one of his favorite ways of punishing people was by impaling them.";
            break;
        case 1:
            RODidYouKnowPlaceholder.text = @"Fact #2 \n \nEurope’s second largest underground glacier, the Scarisoara glacier, is found underneath the Bihor Mountains in Romania. It has a volume of 75,000 cubic meters and has existed for more than 3,500 years.";
            break;
        case 2:
            RODidYouKnowPlaceholder.text = @"Fact #3 \n \nThe scientist who discovered insulin was Nicolae Paulescu, a Romanian, who originally called it pancreine. Although two Canadian scientists were awarded the Nobel Prize in 1923 for their study of insulin, Paulescu’s pioneering work in the field of diabetic medicine was duly accredited.";
            break;
        case 3:
            RODidYouKnowPlaceholder.text = @"Fact #4 \n \nThe actor who first played the role of Tarzan in a Hollywood movie was a Romanian. Johnny Weissmuller, who starred in Tarzan the Ape Man in 1932, was born in Freidorf, Timisoara in Romania.";
            break;
        case 4:
            RODidYouKnowPlaceholder.text = @"Fact #5 \n \nThe first ever perfect score of 10 in gymnastics was given to Romanian gymnast Nadia Comaneci. She bagged the score after her performance in a competition held in Montreal, Canada in 1976.";
            break;
        case 5:
            RODidYouKnowPlaceholder.text = @"Fact #6 \n \nThe scientist who discovered insulin was Nicolae Paulescu, a Romanian, who originally called it pancreine. Although two Canadian scientists were awarded the Nobel Prize in 1923 for their study of insulin, Paulescu’s pioneering work in the field of diabetic medicine was duly accredited.";
            break;
        case 6:
            RODidYouKnowPlaceholder.text = @"Fact #7 \n \nThe city of Timisoara in Romania is the birth place of the horse tram and the electric street light. These inventions were first introduced in 1869 and 1889 respectively.";
            break;
        default:
            break;
    }
}

- (IBAction)RODidYouKnowBasic:(id)sender {
    romanianBasic *ROBasic = [[romanianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ROBasic animated:NO completion:NULL];
}

- (IBAction)RODidYouKnowQuiz:(id)sender {
    romanianQuiz *ROQuiz = [[romanianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ROQuiz animated:NO completion:NULL];
}
@end
