//
//  ukrainianBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ukrainianBasic : UIViewController {
    __weak IBOutlet UIImageView *UABasicIMGPlaceholder;
    __weak IBOutlet UILabel *UABasicTXTPlaceholder;
    __weak IBOutlet UIButton *UABasicPlayButton;
    __weak IBOutlet UILabel *UABasicTut1;
    __weak IBOutlet UIButton *UABasicTut2;
    __weak IBOutlet UILabel *UABasicTut3;
    __weak IBOutlet UILabel *UABasicTut4;
}
- (IBAction)doneUABasic:(id)sender;
- (IBAction)randomUABasic:(id)sender;
- (IBAction)UABasicDidYouKnow:(id)sender;
- (IBAction)UABasicQuiz:(id)sender;
- (IBAction)UAPlay:(id)sender;

@end
