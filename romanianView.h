//
//  romanianView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface romanianView : UIViewController {
}
- (IBAction)doneRomanian:(id)sender;
- (IBAction)randomRomanian:(id)sender;
- (IBAction)switchRODidYouKnow:(id)sender;
- (IBAction)switchROBasic:(id)sender;
- (IBAction)switchROQuiz:(id)sender;
@end
