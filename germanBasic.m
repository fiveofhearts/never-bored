//
//  germanBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "germanBasic.h"
#import "germanDidYouKnow.h"
#import "germanQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface germanBasic ()

@end

@implementation germanBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneDEBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomDEBasic:(id)sender {
    DEBasicTut1.hidden = YES;
    DEBasicTut2.hidden = YES;
    DEBasicTut3.hidden = YES;
    DEBasicTut4.hidden = YES;
    DEBasicIMGPlaceholder.hidden = NO;
    DEBasicPlayButton.hidden = NO;
    DEBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            DEBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            DEBasicTXTPlaceholder.text = @"Auf wiedersehen";
            break;
        case 1:
            DEBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            DEBasicTXTPlaceholder.text = @"Guten tag";
            break;
        case 2:
            DEBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            DEBasicTXTPlaceholder.text = @"Mein name ist";
            break;
        case 3:
            DEBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            DEBasicTXTPlaceholder.text = @"Angenehm";
            break;
        case 4:
            DEBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            DEBasicTXTPlaceholder.text = @"Nein";
            break;
        case 5:
            DEBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            DEBasicTXTPlaceholder.text = @"Danke";
            break;
        case 6:
            DEBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            DEBasicTXTPlaceholder.text = @"Ja";
            break;
        default:
            break;
    }
}

- (IBAction)DEBasicDidYouKnow:(id)sender {
    germanDidYouKnow *DEDidYouKnow = [[germanDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEDidYouKnow animated:NO completion:NULL];
}

- (IBAction)DEBasicQuiz:(id)sender {
    germanQuiz *DEQuiz = [[germanQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEQuiz animated:NO completion:NULL];
}

- (IBAction)DEPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([DEBasicTXTPlaceholder.text isEqual: @"Auf wiedersehen"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeGerman", CFSTR ("mp3"), NULL);
    }
    else if ([DEBasicTXTPlaceholder.text isEqual: @"Guten tag"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiGerman", CFSTR ("mp3"), NULL);
    }
    else if ([DEBasicTXTPlaceholder.text isEqual: @"Mein name ist"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsGerman", CFSTR ("mp3"), NULL);
    }
    else if ([DEBasicTXTPlaceholder.text isEqual: @"Angenehm"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouGerman", CFSTR ("mp3"), NULL);
    }
    else if ([DEBasicTXTPlaceholder.text isEqual: @"Nein"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noGerman", CFSTR ("mp3"), NULL);
    }
    else if ([DEBasicTXTPlaceholder.text isEqual: @"Danke"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouGerman", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesGerman", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end