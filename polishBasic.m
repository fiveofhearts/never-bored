//
//  polishBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "polishBasic.h"
#import "polishDidYouKnow.h"
#import "polishQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface polishBasic ()

@end

@implementation polishBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)donePLBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomPLBasic:(id)sender {
    PLBasicTut1.hidden = YES;
    PLBasicTut2.hidden = YES;
    PLBasicTut3.hidden = YES;
    PLBasicTut4.hidden = YES;
    PLBasicIMGPlaceholder.hidden = NO;
    PLBasicPlayButton.hidden = NO;
    PLBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            PLBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            PLBasicTXTPlaceholder.text = @"Do widzenia";
            break;
        case 1:
            PLBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            PLBasicTXTPlaceholder.text = @"Cześć";
            break;
        case 2:
            PLBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            PLBasicTXTPlaceholder.text = @"Nazywam się";
            break;
        case 3:
            PLBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            PLBasicTXTPlaceholder.text = @"Bardzo mi miło";
            break;
        case 4:
            PLBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            PLBasicTXTPlaceholder.text = @"Nie";
            break;
        case 5:
            PLBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            PLBasicTXTPlaceholder.text = @"Dziękuję";
            break;
        case 6:
            PLBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            PLBasicTXTPlaceholder.text = @"Tak";
            break;
        default:
            break;
    }
}

- (IBAction)PLBasicDidYouKnow:(id)sender {
    polishDidYouKnow *PLDidYouKnow = [[polishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLDidYouKnow animated:NO completion:NULL];
}

- (IBAction)PLBasicQuiz:(id)sender {
    polishQuiz *PLQuiz = [[polishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLQuiz animated:NO completion:NULL];
}

- (IBAction)PLPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([PLBasicTXTPlaceholder.text isEqual: @"Do widzenia"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyePolish", CFSTR ("mp3"), NULL);
    }
    else if ([PLBasicTXTPlaceholder.text isEqual: @"Cześć"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiPolish", CFSTR ("mp3"), NULL);
    }
    else if ([PLBasicTXTPlaceholder.text isEqual: @"Nazywam się"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsPolish", CFSTR ("mp3"), NULL);
    }
    else if ([PLBasicTXTPlaceholder.text isEqual: @"Bardzo mi miło"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouPolish", CFSTR ("mp3"), NULL);
    }
    else if ([PLBasicTXTPlaceholder.text isEqual: @"Nie"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noPolish", CFSTR ("mp3"), NULL);
    }
    else if ([PLBasicTXTPlaceholder.text isEqual: @"Dziękuję"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouPolish", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesPolish", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end
