//
//  dutchDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "dutchDidYouKnow.h"
#import "dutchBasic.h"
#import "dutchQuiz.h"

@interface dutchDidYouKnow ()

@end

@implementation dutchDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneNLDidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomNLDidYouKNow:(id)sender {
    NLDidYouKnowTut1.hidden = YES;
    NLDidYouKnowTut2.hidden = YES;
    NLDidYouKnowTut3.hidden = YES;
    NLDidYouKnowTut4.hidden = YES;
    NLDidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            NLDidYouKnowPlaceholder.text = @"Fact #1 \n \nThe herring with chopped raw onions and pickles is a national dish. The Dutch consume 12 million kilograms every year, which translates to at least 5 fish per person.";
            break;
        case 1:
            NLDidYouKnowPlaceholder.text = @"Fact #2 \n \nNetherlands and Holland are not synonymous. Holland is largely the western coastal region of the Netherlands, comprising of Amsterdam, Rotterdam, Haarlem, Leiden and the Hague.";
            break;
        case 2:
            NLDidYouKnowPlaceholder.text = @"Fact #3 \n \nThe first country to legalise same sex marriages in 2001.";
            break;
        case 3:
            NLDidYouKnowPlaceholder.text = @"Fact #4 \n \nThe Dutch are the world experts on keeping back water from the sea and rivers turning Netherlands into an Atlantis. The US government turned to the Dutch for help during the hurricane Katrina disaster.";
            break;
        case 4:
            NLDidYouKnowPlaceholder.text = @"Fact #5 \n \nGin was invented in the Netherlands. Called 'jenever', it was originally used for medicinal purposes in the 16th century.";
            break;
        case 5:
            NLDidYouKnowPlaceholder.text = @"Fact #6 \n \nThe Schiphol Airport is actually 4.5 m below sea level.";
            break;
        case 6:
            NLDidYouKnowPlaceholder.text = @"Fact #7 \n \nIt has more than 4,000 km of navigable canals, rivers and lakes.";
            break;
        default:
            break;
    }
}

- (IBAction)NLDidYouKnowBasic:(id)sender {
    dutchBasic *NLBasic = [[dutchBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:NLBasic animated:NO completion:NULL];
}

- (IBAction)NLDidYouKnowQuiz:(id)sender {
    dutchQuiz *NLQuiz = [[dutchQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:NLQuiz animated:NO completion:NULL];
}
@end
