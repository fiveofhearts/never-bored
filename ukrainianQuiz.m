//
//  ukrainianQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "ukrainianQuiz.h"
#import "ukrainianDidYouKnow.h"
#import "ukrainianBasic.h"

@interface ukrainianQuiz ()

@end

@implementation ukrainianQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTRQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomTRQuiz:(id)sender {
    TRQuizTut1.hidden = YES;
    TRQuizTut2.hidden = YES;
    TRQuizTut3.hidden = YES;
    TRQuizTut4.hidden = YES;
    TRQuizQuestion.hidden = NO;
    TRQuizButtonA.hidden = NO;
    TRQuizButtonB.hidden = NO;
    TRQuizButtonC.hidden = NO;
    TRQuizButtonD.hidden = NO;
    TRQuizAnswer1Label.hidden = NO;
    TRQuizAnswer2Label.hidden = NO;
    TRQuizAnswer3Label.hidden = NO;
    TRQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            TRQuizQuestion.text = @"The ... most visited McDonald’s in the world is located in Kiev.";
            TRQuizAnswer1Label.text = @"third";
            TRQuizAnswer2Label.text = @"second";
            TRQuizAnswer3Label.text = @"fourth";
            TRQuizAnswer4Label.text = @"fifth";
            TRQuizResult.text = @"";
            break;
        case 1:
            TRQuizQuestion.text = @"An average Ukrainian eats only ... of pork a year.";
            TRQuizAnswer1Label.text = @"7 kg";
            TRQuizAnswer2Label.text = @"18 kg";
            TRQuizAnswer3Label.text = @"24 kg";
            TRQuizAnswer4Label.text = @"32 kg";
            TRQuizResult.text = @"";
            break;
        case 2:
            TRQuizQuestion.text = @"Ukrainians made the world’s largest glass of champagne – ...";
            TRQuizAnswer1Label.text = @"48.25 liters";
            TRQuizAnswer2Label.text = @"65.25 liters";
            TRQuizAnswer3Label.text = @"56.25 liters";
            TRQuizAnswer4Label.text = @"67.25 liters";
            TRQuizResult.text = @"";
            break;
        case 3:
            TRQuizQuestion.text = @"The coin named “10 years of hryvnia revival” weighs exactly...";
            TRQuizAnswer1Label.text = @"two kilograms";
            TRQuizAnswer2Label.text = @"three kilograms";
            TRQuizAnswer3Label.text = @"four kilograms";
            TRQuizAnswer4Label.text = @"one kilogram";
            TRQuizResult.text = @"";
            break;
        case 4:
            TRQuizQuestion.text = @"Мене звуть means ... in Ukrainian.";
            TRQuizAnswer1Label.text = @"Hello";
            TRQuizAnswer2Label.text = @"Goodbye";
            TRQuizAnswer3Label.text = @"Yes";
            TRQuizAnswer4Label.text = @"My name is";
            TRQuizResult.text = @"";
            break;
        case 5:
            TRQuizQuestion.text = @"Добрий день means ... in Ukrainian.";
            TRQuizAnswer1Label.text = @"Goodbye";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Hello";
            TRQuizAnswer4Label.text = @"No";
            TRQuizResult.text = @"";
            break;
        case 6:
            TRQuizQuestion.text = @"Дякую means ... in Ukrainian.";
            TRQuizAnswer1Label.text = @"Yes";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Goodbye";
            TRQuizAnswer4Label.text = @"Hello";
            TRQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)TRQuizDidYouKnow:(id)sender {
    ukrainianDidYouKnow *UADidYouKnow = [[ukrainianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UADidYouKnow animated:NO completion:NULL];
}

- (IBAction)TRQuizBasic:(id)sender {
    ukrainianBasic *UABasic = [[ukrainianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UABasic animated:NO completion:NULL];
}

- (IBAction)TRQuizAnswer1:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"The ... most visited McDonald’s in the world is located in Kiev."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"An average Ukrainian eats only ... of pork a year."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Ukrainians made the world’s largest glass of champagne – ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The coin named “10 years of hryvnia revival” weighs exactly..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Мене звуть means ... in Ukrainian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Добрий день means ... in Ukrainian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer2:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"The ... most visited McDonald’s in the world is located in Kiev."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"An average Ukrainian eats only ... of pork a year."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Ukrainians made the world’s largest glass of champagne – ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The coin named “10 years of hryvnia revival” weighs exactly..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Мене звуть means ... in Ukrainian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Добрий день means ... in Ukrainian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)TRQuizAnswer3:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"The ... most visited McDonald’s in the world is located in Kiev."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"An average Ukrainian eats only ... of pork a year."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Ukrainians made the world’s largest glass of champagne – ..."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"The coin named “10 years of hryvnia revival” weighs exactly..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Мене звуть means ... in Ukrainian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Добрий день means ... in Ukrainian."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer4:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"The ... most visited McDonald’s in the world is located in Kiev."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"An average Ukrainian eats only ... of pork a year."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Ukrainians made the world’s largest glass of champagne – ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The coin named “10 years of hryvnia revival” weighs exactly..."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Мене звуть means ... in Ukrainian."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Добрий день means ... in Ukrainian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
}
@end
