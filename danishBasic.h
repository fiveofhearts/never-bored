//
//  danishBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface danishBasic : UIViewController {
    __weak IBOutlet UIImageView *DKBasicIMGPlaceholder;
    __weak IBOutlet UILabel *DKBasicTXTPlaceholder;
    __weak IBOutlet UIButton *DKBasicPlayButton;
    __weak IBOutlet UILabel *DKBasicTut1;
    __weak IBOutlet UIButton *DKBasicTut2;
    __weak IBOutlet UILabel *DKBasicTut3;
    __weak IBOutlet UILabel *DKBasicTut4;
}
- (IBAction)doneDKBasic:(id)sender;
- (IBAction)randomDKBasic:(id)sender;
- (IBAction)DKBasicDidYouKnow:(id)sender;
- (IBAction)DKBasicQuiz:(id)sender;
- (IBAction)DKPlay:(id)sender;

@end
