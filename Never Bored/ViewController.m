//
//  ViewController.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "ViewController.h"
#import "readView.h"
#import "watchView.h"
#import "playView.h"
#import "learnView.h"
#import <Social/Social.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"switchRandomButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(switchRandom) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(96.0, 135.0, 128.0, 128.0);
    button.clipsToBounds = YES;
    button.layer.cornerRadius = 64;
    [self.view addSubview:button];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchRead:(id)sender {
    readView *read = [[readView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:read animated:YES completion:NULL];
}

- (IBAction)switchWatch:(id)sender {
    watchView *watch = [[watchView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:watch animated:YES completion:NULL];
}

- (IBAction)switchPlay:(id)sender {
    playView *play = [[playView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:play animated:YES completion:NULL];
}

- (IBAction)switchLearn:(id)sender {
    learnView *learn = [[learnView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:learn animated:YES completion:NULL];
}

- (void)switchRandom
{
    int i = arc4random() % 4;
    if (i == 1) {
        readView *read = [[readView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:read animated:YES completion:NULL];
    }
    else if (i == 2) {
        watchView *watch = [[watchView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:watch animated:YES completion:NULL];
    }
    else if (i == 3) {
        playView *play = [[playView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:play animated:YES completion:NULL];
    }
    else {
        learnView *learn = [[learnView alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:learn animated:YES completion:NULL];
    }
}

- (IBAction)shareOnFacebook:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookSheet setInitialText:@"Best App I've downloaded in a while. It's called Never Bored and you can find it in the App Store. #NeverBored"];
        [self presentViewController:facebookSheet animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You can't share a post right now, make sure you have at least one Facebok account set." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
    
}

- (IBAction)shareOnTwitter:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"Best App I've downloaded in a while. It's called Never Bored and you can find it in the App Store. #NeverBored"];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You can't send a tweet right now, make sure you have at least one Twitter account set." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
}
@end
