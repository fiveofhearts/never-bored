//
//  ViewController.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//  {}

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ViewController : UIViewController {
}
- (IBAction)switchRead:(id)sender;
- (IBAction)switchWatch:(id)sender;
- (IBAction)switchPlay:(id)sender;
- (IBAction)switchLearn:(id)sender;
- (IBAction)shareOnFacebook:(id)sender;
- (IBAction)shareOnTwitter:(id)sender;
@end
