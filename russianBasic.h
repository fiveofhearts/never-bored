//
//  russianBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface russianBasic : UIViewController {
    __weak IBOutlet UIImageView *RUBasicIMGPlaceholder;
    __weak IBOutlet UILabel *RUBasicTXTPlaceholder;
    __weak IBOutlet UIButton *RUBasicPlayButton;
    __weak IBOutlet UILabel *RUBasicTut1;
    __weak IBOutlet UIButton *RUBasicTut2;
    __weak IBOutlet UILabel *RUBasicTut3;
    __weak IBOutlet UILabel *RUBasicTut4;
}
- (IBAction)doneRUBasic:(id)sender;
- (IBAction)randomRUBasic:(id)sender;
- (IBAction)RUBasicDidYouKnow:(id)sender;
- (IBAction)RUBasicQuiz:(id)sender;
- (IBAction)RUPlay:(id)sender;

@end