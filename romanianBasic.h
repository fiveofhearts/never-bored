//
//  romanianBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface romanianBasic : UIViewController {
    __weak IBOutlet UIImageView *ROBasicIMGPlaceholder;
    __weak IBOutlet UILabel *ROBasicTXTPlaceholder;
    __weak IBOutlet UIButton *ROBasicPlayButton;
    __weak IBOutlet UILabel *ROBasicTut1;
    __weak IBOutlet UIButton *ROBasicTut2;
    __weak IBOutlet UILabel *ROBasicTut3;
    __weak IBOutlet UILabel *ROBasicTut4;
}
- (IBAction)doneROBasic:(id)sender;
- (IBAction)randomROBasic:(id)sender;
- (IBAction)ROBasicDidYouKnow:(id)sender;
- (IBAction)ROBasicQuiz:(id)sender;
- (IBAction)ROPlay:(id)sender;

@end
