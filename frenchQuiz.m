//
//  frenchQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "frenchQuiz.h"
#import "frenchDidYouKnow.h"
#import "frenchBasic.h"

@interface frenchQuiz ()

@end

@implementation frenchQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneFRQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomFRQuiz:(id)sender {
    FRQuizTut1.hidden = YES;
    FRQuizTut2.hidden = YES;
    FRQuizTut3.hidden = YES;
    FRQuizTut4.hidden = YES;
    FRQuizQuestion.hidden = NO;
    FRQuizButtonA.hidden = NO;
    FRQuizButtonB.hidden = NO;
    FRQuizButtonC.hidden = NO;
    FRQuizButtonD.hidden = NO;
    FRQuizAnswer1Label.hidden = NO;
    FRQuizAnswer2Label.hidden = NO;
    FRQuizAnswer3Label.hidden = NO;
    FRQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            FRQuizQuestion.text = @"France has the highest number of ...";
            FRQuizAnswer1Label.text = @"Sky resorts";
            FRQuizAnswer2Label.text = @"Pools";
            FRQuizAnswer3Label.text = @"Lakes";
            FRQuizAnswer4Label.text = @"Bars";
            FRQuizResult.text = @"";
            break;
        case 1:
            FRQuizQuestion.text = @"How many tourists visit the France each year?";
            FRQuizAnswer1Label.text = @"70 million";
            FRQuizAnswer2Label.text = @"75 million";
            FRQuizAnswer3Label.text = @"80 million";
            FRQuizAnswer4Label.text = @"85 million";
            FRQuizResult.text = @"";
            break;
        case 2:
            FRQuizQuestion.text = @"France has won the most Nobel Prizes for ...";
            FRQuizAnswer1Label.text = @"Economics";
            FRQuizAnswer2Label.text = @"Chemistry";
            FRQuizAnswer3Label.text = @"Literature";
            FRQuizAnswer4Label.text = @"Physics";
            FRQuizResult.text = @"";
            break;
        case 3:
            FRQuizQuestion.text = @"The most visited attraction in Paris is ...";
            FRQuizAnswer1Label.text = @"The Eiffel Tower";
            FRQuizAnswer2Label.text = @"The Louvre";
            FRQuizAnswer3Label.text = @"The Royal Palace";
            FRQuizAnswer4Label.text = @"Disneyland";
            FRQuizResult.text = @"";
            break;
        case 4:
            FRQuizQuestion.text = @"Je m'appelle means ... in French.";
            FRQuizAnswer1Label.text = @"Hello";
            FRQuizAnswer2Label.text = @"Goodbye";
            FRQuizAnswer3Label.text = @"Yes";
            FRQuizAnswer4Label.text = @"My name is";
            FRQuizResult.text = @"";
            break;
        case 5:
            FRQuizQuestion.text = @"Bonjour means ... in French.";
            FRQuizAnswer1Label.text = @"Goodbye";
            FRQuizAnswer2Label.text = @"Thank you";
            FRQuizAnswer3Label.text = @"Hello";
            FRQuizAnswer4Label.text = @"No";
            FRQuizResult.text = @"";
            break;
        case 6:
            FRQuizQuestion.text = @"Merci means ... in French.";
            FRQuizAnswer1Label.text = @"Yes";
            FRQuizAnswer2Label.text = @"Thank you";
            FRQuizAnswer3Label.text = @"Goodbye";
            FRQuizAnswer4Label.text = @"Hello";
            FRQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)FRQuizDidYouKnow:(id)sender {
    frenchDidYouKnow *FRDidYouKnow = [[frenchDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRDidYouKnow animated:NO completion:NULL];
}

- (IBAction)FRQuizBasic:(id)sender {
    frenchBasic *FRBasic = [[frenchBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRBasic animated:NO completion:NULL];
}

- (IBAction)FRQuizAnswer1:(id)sender {
    if ([FRQuizQuestion.text isEqual: @"France has the highest number of ..."]) {
        FRQuizResult.text = @"Correct!";
        FRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([FRQuizQuestion.text isEqual: @"How many tourists visit the France each year?"]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer1Label.hidden = YES;
        FRQuizButtonA.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"France has won the most Nobel Prizes for ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer1Label.hidden = YES;
        FRQuizButtonA.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"The most visited attraction in Paris is ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer1Label.hidden = YES;
        FRQuizButtonA.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"Je m'appelle means ... in French."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer1Label.hidden = YES;
        FRQuizButtonA.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"Bonjour means ... in French."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer1Label.hidden = YES;
        FRQuizButtonA.hidden = YES;
    }
    else {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer1Label.hidden = YES;
        FRQuizButtonA.hidden = YES;
    }
}

- (IBAction)FRQuizAnswer2:(id)sender {
    if ([FRQuizQuestion.text isEqual: @"France has the highest number of ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer2Label.hidden = YES;
        FRQuizButtonB.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"How many tourists visit the France each year?"]) {
        FRQuizResult.text = @"Correct!";
        FRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([FRQuizQuestion.text isEqual: @"France has won the most Nobel Prizes for ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer2Label.hidden = YES;
        FRQuizButtonB.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"The most visited attraction in Paris is ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer2Label.hidden = YES;
        FRQuizButtonB.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"Je m'appelle means ... in French."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer2Label.hidden = YES;
        FRQuizButtonB.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"Bonjour means ... in French."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer2Label.hidden = YES;
        FRQuizButtonB.hidden = YES;
    }
    else {
        FRQuizResult.text = @"Correct!";
        FRQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)FRQuizAnswer3:(id)sender {
    if ([FRQuizQuestion.text isEqual: @"France has the highest number of ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer3Label.hidden = YES;
        FRQuizButtonC.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"How many tourists visit the France each year?"]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer3Label.hidden = YES;
        FRQuizButtonC.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"France has won the most Nobel Prizes for ..."]) {
        FRQuizResult.text = @"Correct!";
        FRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([FRQuizQuestion.text isEqual: @"The most visited attraction in Paris is ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer3Label.hidden = YES;
        FRQuizButtonC.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"Je m'appelle means ... in French."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer3Label.hidden = YES;
        FRQuizButtonC.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"Bonjour means ... in French."]) {
        FRQuizResult.text = @"Correct!";
        FRQuizResult.textColor = [UIColor greenColor];
    }
    else {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer3Label.hidden = YES;
        FRQuizButtonC.hidden = YES;
    }
}

- (IBAction)FRQuizAnswer4:(id)sender {
    if ([FRQuizQuestion.text isEqual: @"France has the highest number of ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer4Label.hidden = YES;
        FRQuizButtonD.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"How many tourists visit the France each year?"]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer4Label.hidden = YES;
        FRQuizButtonD.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"France has won the most Nobel Prizes for ..."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer4Label.hidden = YES;
        FRQuizButtonD.hidden = YES;
    }
    else if ([FRQuizQuestion.text isEqual: @"The most visited attraction in Paris is ..."]) {
        FRQuizResult.text = @"Correct!";
        FRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([FRQuizQuestion.text isEqual: @"Je m'appelle means ... in French."]) {
        FRQuizResult.text = @"Correct!";
        FRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([FRQuizQuestion.text isEqual: @"Bonjour means ... in French."]) {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer4Label.hidden = YES;
        FRQuizButtonD.hidden = YES;
    }
    else {
        FRQuizResult.text = @"Wrong!";
        FRQuizResult.textColor = [UIColor redColor];
        FRQuizAnswer4Label.hidden = YES;
        FRQuizButtonD.hidden = YES;
    }
}
@end