//
//  dutchView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "dutchView.h"
#import "dutchDidYouKnow.h"
#import "dutchBasic.h"
#import "dutchQuiz.h"

@interface dutchView ()

@end

@implementation dutchView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneDutch:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomDutch:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        dutchDidYouKnow *NLDidYouKnow = [[dutchDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:NLDidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        dutchBasic *NLBasic = [[dutchBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:NLBasic animated:NO completion:NULL];
    }
    else {
        dutchQuiz *NLQuiz = [[dutchQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:NLQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchNLDidYouKnow:(id)sender {
    dutchDidYouKnow *NLDidYouKnow = [[dutchDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:NLDidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchNLBasic:(id)sender {
    dutchBasic *NLBasic = [[dutchBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:NLBasic animated:NO completion:NULL];
}

- (IBAction)switchNLQuiz:(id)sender {
    dutchQuiz *NLQuiz = [[dutchQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:NLQuiz animated:NO completion:NULL];
}
@end
