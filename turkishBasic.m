//
//  turkishBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "turkishBasic.h"
#import "turkishDidYouKnow.h"
#import "turkishQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface turkishBasic ()

@end

@implementation turkishBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTRBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomTRBasic:(id)sender {
    TRBasicTut1.hidden = YES;
    TRBasicTut2.hidden = YES;
    TRBasicTut3.hidden = YES;
    TRBasicTut4.hidden = YES;
    TRBasicIMGPlaceholder.hidden = NO;
    TRBasicPlayButton.hidden = NO;
    TRBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            TRBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            TRBasicTXTPlaceholder.text = @"Hoşçakalın";
            break;
        case 1:
            TRBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            TRBasicTXTPlaceholder.text = @"Merhaba";
            break;
        case 2:
            TRBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            TRBasicTXTPlaceholder.text = @"Benim adım";
            break;
        case 3:
            TRBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            TRBasicTXTPlaceholder.text = @"Tanıştığımıza memnum oldum";
            break;
        case 4:
            TRBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            TRBasicTXTPlaceholder.text = @"Hayır";
            break;
        case 5:
            TRBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            TRBasicTXTPlaceholder.text = @"Teşekkür ederim";
            break;
        case 6:
            TRBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            TRBasicTXTPlaceholder.text = @"Evet";
            break;
        default:
            break;
    }
}

- (IBAction)TRBasicDidYouKnow:(id)sender {
    turkishDidYouKnow *TRDidYouKnow = [[turkishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRDidYouKnow animated:NO completion:NULL];
}

- (IBAction)TRBasicQuiz:(id)sender {
    turkishQuiz *TRQuiz = [[turkishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:TRQuiz animated:NO completion:NULL];
}

- (IBAction)TRPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([TRBasicTXTPlaceholder.text isEqual: @"Hoşçakalın"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeTurkish", CFSTR ("mp3"), NULL);
    }
    else if ([TRBasicTXTPlaceholder.text isEqual: @"Merhaba"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiTurkish", CFSTR ("mp3"), NULL);
    }
    else if ([TRBasicTXTPlaceholder.text isEqual: @"Benim adım"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsTurkish", CFSTR ("mp3"), NULL);
    }
    else if ([TRBasicTXTPlaceholder.text isEqual: @"Tanıştığımıza memnum oldum"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouTurkish", CFSTR ("mp3"), NULL);
    }
    else if ([TRBasicTXTPlaceholder.text isEqual: @"Hayır"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noTurkish", CFSTR ("mp3"), NULL);
    }
    else if ([TRBasicTXTPlaceholder.text isEqual: @"Teşekkür ederim"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouTurkish", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesTurkish", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end