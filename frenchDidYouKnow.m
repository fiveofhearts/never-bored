//
//  frenchDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "frenchDidYouKnow.h"
#import "frenchBasic.h"
#import "frenchQuiz.h"

@interface frenchDidYouKnow ()

@end

@implementation frenchDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneFRDidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomFRDidYouKNow:(id)sender {
    FRDidYouKnowTut1.hidden = YES;
    FRDidYouKnowTut2.hidden = YES;
    FRDidYouKnowTut3.hidden = YES;
    FRDidYouKnowTut4.hidden = YES;
    FRDidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            FRDidYouKnowPlaceholder.text = @"Fact #1 \n \nFrance has the highest number of ski resorts.";
            break;
        case 1:
            FRDidYouKnowPlaceholder.text = @"Fact #2 \n \nFrance is the most visited country in the world with 75 million tourists yearly.";
            break;
        case 2:
            FRDidYouKnowPlaceholder.text = @"Fact #3 \n \nFrance has won the most Nobel Prizes for Literature than any other country in the world, and the second most in mathematics.";
            break;
        case 3:
            FRDidYouKnowPlaceholder.text = @"Fact #4 \n \nFamous French inventions include: the hot air balloon, the submarine, and the parachute.";
            break;
        case 4:
            FRDidYouKnowPlaceholder.text = @"Fact #5 \n \nParis, which is nicknamed “city of lights”, actually refers to the number of intellectuals who live there.";
            break;
        case 5:
            FRDidYouKnowPlaceholder.text = @"Fact #6 \n \nThe most visited attraction in Paris isn't the Eiffel Tower (5.5 million), or the Louvre (5 million), but Disneyland Paris at 13 million people.";
            break;
        case 6:
            FRDidYouKnowPlaceholder.text = @"Fact #7 \n \nFrench toast and french fries aren’t French inventions.";
            break;
        default:
            break;
    }
}

- (IBAction)FRDidYouKnowBasic:(id)sender {
    frenchBasic *FRBasic = [[frenchBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRBasic animated:NO completion:NULL];
}

- (IBAction)FRDidYouKnowQuiz:(id)sender {
    frenchQuiz *FRQuiz = [[frenchQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:FRQuiz animated:NO completion:NULL];
}
@end
