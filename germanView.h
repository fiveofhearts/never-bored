//
//  germanView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface germanView : UIViewController {
}
- (IBAction)doneGerman:(id)sender;
- (IBAction)randomGerman:(id)sender;
- (IBAction)switchDEDidYouKnow:(id)sender;
- (IBAction)switchDEBasic:(id)sender;
- (IBAction)switchDEQuiz:(id)sender;
@end
