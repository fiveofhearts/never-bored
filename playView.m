//
//  playView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 9/30/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "playView.h"
#import "pongView.h"
#import "ticTacToeView.h"
#import "findTheBeeView.h"

@interface playView ()

@end

@implementation playView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)donePlay:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomPlay:(id)sender {
}

- (IBAction)switchPong:(id)sender {
    pongView *pong = [[pongView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:pong animated:YES completion:NULL];
}

- (IBAction)switchFindTheBee:(id)sender {
    findTheBeeView *findTheBee = [[findTheBeeView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:findTheBee animated:YES completion:NULL];
}

- (IBAction)switchTicTacToe:(id)sender {
    ticTacToeView *ticTacToe = [[ticTacToeView alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ticTacToe animated:YES completion:NULL];
}

@end
