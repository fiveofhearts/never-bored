//
//  russianQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "russianQuiz.h"
#import "russianDidYouKnow.h"
#import "russianBasic.h"

@interface russianQuiz ()

@end

@implementation russianQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneRUQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomRUQuiz:(id)sender {
    RUQuizTut1.hidden = YES;
    RUQuizTut2.hidden = YES;
    RUQuizTut3.hidden = YES;
    RUQuizTut4.hidden = YES;
    RUQuizQuestion.hidden = NO;
    RUQuizButtonA.hidden = NO;
    RUQuizButtonB.hidden = NO;
    RUQuizButtonC.hidden = NO;
    RUQuizButtonD.hidden = NO;
    RUQuizAnswer1Label.hidden = NO;
    RUQuizAnswer2Label.hidden = NO;
    RUQuizAnswer3Label.hidden = NO;
    RUQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            RUQuizQuestion.text = @"How long is the Trans-Siberian Railway?";
            RUQuizAnswer1Label.text = @"9200 km";
            RUQuizAnswer2Label.text = @"8200 km";
            RUQuizAnswer3Label.text = @"7200 km";
            RUQuizAnswer4Label.text = @"6200 km";
            RUQuizResult.text = @"";
            break;
        case 1:
            RUQuizQuestion.text = @"How many millions ride the Moscow's Subway System?";
            RUQuizAnswer1Label.text = @"Over 6 million";
            RUQuizAnswer2Label.text = @"Over 9 million";
            RUQuizAnswer3Label.text = @"Over 7 million";
            RUQuizAnswer4Label.text = @"Over 8 million";
            RUQuizResult.text = @"";
            break;
        case 2:
            RUQuizQuestion.text = @"How many trains operate in the Moscow's Subway System?";
            RUQuizAnswer1Label.text = @"9925";
            RUQuizAnswer2Label.text = @"9815";
            RUQuizAnswer3Label.text = @"9915";
            RUQuizAnswer4Label.text = @"9825";
            RUQuizResult.text = @"";
            break;
        case 3:
            RUQuizQuestion.text = @"How many seas are there in Russia?";
            RUQuizAnswer1Label.text = @"10";
            RUQuizAnswer2Label.text = @"11";
            RUQuizAnswer3Label.text = @"13";
            RUQuizAnswer4Label.text = @"12";
            RUQuizResult.text = @"";
            break;
        case 4:
            RUQuizQuestion.text = @"меня зовут means ... in Russian.";
            RUQuizAnswer1Label.text = @"Hello";
            RUQuizAnswer2Label.text = @"Goodbye";
            RUQuizAnswer3Label.text = @"Yes";
            RUQuizAnswer4Label.text = @"My name is";
            RUQuizResult.text = @"";
            break;
        case 5:
            RUQuizQuestion.text = @"Здравствуйте means ... in Russian.";
            RUQuizAnswer1Label.text = @"Goodbye";
            RUQuizAnswer2Label.text = @"Thank you";
            RUQuizAnswer3Label.text = @"Hello";
            RUQuizAnswer4Label.text = @"No";
            RUQuizResult.text = @"";
            break;
        case 6:
            RUQuizQuestion.text = @"спасибо means ... in Russian.";
            RUQuizAnswer1Label.text = @"Yes";
            RUQuizAnswer2Label.text = @"Thank you";
            RUQuizAnswer3Label.text = @"Goodbye";
            RUQuizAnswer4Label.text = @"Hello";
            RUQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)RUQuizDidYouKnow:(id)sender {
    russianDidYouKnow *RUDidYouKnow = [[russianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUDidYouKnow animated:NO completion:NULL];
}

- (IBAction)RUQuizBasic:(id)sender {
    russianBasic *RUBasic = [[russianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RUBasic animated:NO completion:NULL];
}

- (IBAction)RUQuizAnswer1:(id)sender {
    if ([RUQuizQuestion.text isEqual: @"How long is the Trans-Siberian Railway?"]) {
        RUQuizResult.text = @"Correct!";
        RUQuizResult.textColor = [UIColor greenColor];
    }
    else if ([RUQuizQuestion.text isEqual: @"How many millions ride the Moscow's Subway System?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer1Label.hidden = YES;
        RUQuizButtonA.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many trains operate in the Moscow's Subway System?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer1Label.hidden = YES;
        RUQuizButtonA.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many seas are there in Russia?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer1Label.hidden = YES;
        RUQuizButtonA.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"меня зовут means ... in Russian."]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer1Label.hidden = YES;
        RUQuizButtonA.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"Здравствуйте means ... in Russian."]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer1Label.hidden = YES;
        RUQuizButtonA.hidden = YES;
    }
    else {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer1Label.hidden = YES;
        RUQuizButtonA.hidden = YES;
    }
}

- (IBAction)RUQuizAnswer2:(id)sender {
    if ([RUQuizQuestion.text isEqual: @"How long is the Trans-Siberian Railway?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer2Label.hidden = YES;
        RUQuizButtonB.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many millions ride the Moscow's Subway System?"]) {
        RUQuizResult.text = @"Correct!";
        RUQuizResult.textColor = [UIColor greenColor];
    }
    else if ([RUQuizQuestion.text isEqual: @"How many trains operate in the Moscow's Subway System?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer2Label.hidden = YES;
        RUQuizButtonB.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many seas are there in Russia?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer2Label.hidden = YES;
        RUQuizButtonB.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"меня зовут means ... in Russian."]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer2Label.hidden = YES;
        RUQuizButtonB.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"Здравствуйте means ... in Russian."]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer2Label.hidden = YES;
        RUQuizButtonB.hidden = YES;
    }
    else {
        RUQuizResult.text = @"Correct!";
        RUQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)RUQuizAnswer3:(id)sender {
    if ([RUQuizQuestion.text isEqual: @"How long is the Trans-Siberian Railway?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer3Label.hidden = YES;
        RUQuizButtonC.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many millions ride the Moscow's Subway System?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer3Label.hidden = YES;
        RUQuizButtonC.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many trains operate in the Moscow's Subway System?"]) {
        RUQuizResult.text = @"Correct!";
        RUQuizResult.textColor = [UIColor greenColor];
    }
    else if ([RUQuizQuestion.text isEqual: @"How many seas are there in Russia?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer3Label.hidden = YES;
        RUQuizButtonC.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"меня зовут means ... in Russian."]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer3Label.hidden = YES;
        RUQuizButtonC.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"Здравствуйте means ... in Russian."]) {
        RUQuizResult.text = @"Correct!";
        RUQuizResult.textColor = [UIColor greenColor];
    }
    else {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer3Label.hidden = YES;
        RUQuizButtonC.hidden = YES;
    }
}

- (IBAction)RUQuizAnswer4:(id)sender {
    if ([RUQuizQuestion.text isEqual: @"How long is the Trans-Siberian Railway?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer4Label.hidden = YES;
        RUQuizButtonD.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many millions ride the Moscow's Subway System?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer4Label.hidden = YES;
        RUQuizButtonD.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many trains operate in the Moscow's Subway System?"]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer4Label.hidden = YES;
        RUQuizButtonD.hidden = YES;
    }
    else if ([RUQuizQuestion.text isEqual: @"How many seas are there in Russia?"]) {
        RUQuizResult.text = @"Correct!";
        RUQuizResult.textColor = [UIColor greenColor];
    }
    else if ([RUQuizQuestion.text isEqual: @"меня зовут means ... in Russian."]) {
        RUQuizResult.text = @"Correct!";
        RUQuizResult.textColor = [UIColor greenColor];
    }
    else if ([RUQuizQuestion.text isEqual: @"Здравствуйте means ... in Russian."]) {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer4Label.hidden = YES;
        RUQuizButtonD.hidden = YES;
    }
    else {
        RUQuizResult.text = @"Wrong!";
        RUQuizResult.textColor = [UIColor redColor];
        RUQuizAnswer4Label.hidden = YES;
        RUQuizButtonD.hidden = YES;
    }
}
@end