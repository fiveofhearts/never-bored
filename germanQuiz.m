//
//  germanQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "germanQuiz.h"
#import "germanDidYouKnow.h"
#import "germanBasic.h"

@interface germanQuiz ()

@end

@implementation germanQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneDEQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomDEQuiz:(id)sender {
    DEQuizTut1.hidden = YES;
    DEQuizTut2.hidden = YES;
    DEQuizTut3.hidden = YES;
    DEQuizTut4.hidden = YES;
    DEQuizQuestion.hidden = NO;
    DEQuizButtonA.hidden = NO;
    DEQuizButtonB.hidden = NO;
    DEQuizButtonC.hidden = NO;
    DEQuizButtonD.hidden = NO;
    DEQuizAnswer1Label.hidden = NO;
    DEQuizAnswer2Label.hidden = NO;
    DEQuizAnswer3Label.hidden = NO;
    DEQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            DEQuizQuestion.text = @"How many Germans speak French fluently?";
            DEQuizAnswer1Label.text = @"3 percent";
            DEQuizAnswer2Label.text = @"2 percent";
            DEQuizAnswer3Label.text = @"4 percent";
            DEQuizAnswer4Label.text = @"5 percent";
            DEQuizResult.text = @"";
            break;
        case 1:
            DEQuizQuestion.text = @"How many Germans do not own a cell phone?";
            DEQuizAnswer1Label.text = @"3 percent";
            DEQuizAnswer2Label.text = @"2 percent";
            DEQuizAnswer3Label.text = @"4 percent";
            DEQuizAnswer4Label.text = @"5 percent";
            DEQuizResult.text = @"";
            break;
        case 2:
            DEQuizQuestion.text = @"Hans Riegel invented ...";
            DEQuizAnswer1Label.text = @"Coca-Cola";
            DEQuizAnswer2Label.text = @"Twix";
            DEQuizAnswer3Label.text = @"Gummy Bears";
            DEQuizAnswer4Label.text = @"Mars";
            DEQuizResult.text = @"";
            break;
        case 3:
            DEQuizQuestion.text = @"How many Germans speak English fluently?";
            DEQuizAnswer1Label.text = @"Almost nobody";
            DEQuizAnswer2Label.text = @"The majority";
            DEQuizAnswer3Label.text = @"Only teenagers";
            DEQuizAnswer4Label.text = @"Nearly half";
            DEQuizResult.text = @"";
            break;
        case 4:
            DEQuizQuestion.text = @"Mein name ist means ... in German.";
            DEQuizAnswer1Label.text = @"Hello";
            DEQuizAnswer2Label.text = @"Goodbye";
            DEQuizAnswer3Label.text = @"Yes";
            DEQuizAnswer4Label.text = @"My name is";
            DEQuizResult.text = @"";
            break;
        case 5:
            DEQuizQuestion.text = @"Guten tag means ... in German.";
            DEQuizAnswer1Label.text = @"Goodbye";
            DEQuizAnswer2Label.text = @"Thank you";
            DEQuizAnswer3Label.text = @"Hello";
            DEQuizAnswer4Label.text = @"No";
            DEQuizResult.text = @"";
            break;
        case 6:
            DEQuizQuestion.text = @"Danke means ... in German.";
            DEQuizAnswer1Label.text = @"Yes";
            DEQuizAnswer2Label.text = @"Thank you";
            DEQuizAnswer3Label.text = @"Goodbye";
            DEQuizAnswer4Label.text = @"Hello";
            DEQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)DEQuizDidYouKnow:(id)sender {
    germanDidYouKnow *DEDidYouKnow = [[germanDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEDidYouKnow animated:NO completion:NULL];
}

- (IBAction)DEQuizBasic:(id)sender {
    germanBasic *DEBasic = [[germanBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DEBasic animated:NO completion:NULL];
}

- (IBAction)DEQuizAnswer1:(id)sender {
    if ([DEQuizQuestion.text isEqual: @"How many Germans speak French fluently?"]) {
        DEQuizResult.text = @"Correct!";
        DEQuizResult.textColor = [UIColor greenColor];
    }
    else if ([DEQuizQuestion.text isEqual: @"How many Germans do not own a cell phone?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer1Label.hidden = YES;
        DEQuizButtonA.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Hans Riegel invented ..."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer1Label.hidden = YES;
        DEQuizButtonA.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"How many Germans speak English fluently?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer1Label.hidden = YES;
        DEQuizButtonA.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Mein name ist means ... in German."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer1Label.hidden = YES;
        DEQuizButtonA.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Guten tag means ... in German."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer1Label.hidden = YES;
        DEQuizButtonA.hidden = YES;
    }
    else {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer1Label.hidden = YES;
        DEQuizButtonA.hidden = YES;
    }
}

- (IBAction)DEQuizAnswer2:(id)sender {
    if ([DEQuizQuestion.text isEqual: @"How many Germans speak French fluently?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer2Label.hidden = YES;
        DEQuizButtonB.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"How many Germans do not own a cell phone?"]) {
        DEQuizResult.text = @"Correct!";
        DEQuizResult.textColor = [UIColor greenColor];
    }
    else if ([DEQuizQuestion.text isEqual: @"Hans Riegel invented ..."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer2Label.hidden = YES;
        DEQuizButtonB.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"How many Germans speak English fluently?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer2Label.hidden = YES;
        DEQuizButtonB.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Mein name ist means ... in German."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer2Label.hidden = YES;
        DEQuizButtonB.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Guten tag means ... in German."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer2Label.hidden = YES;
        DEQuizButtonB.hidden = YES;
    }
    else {
        DEQuizResult.text = @"Correct!";
        DEQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)DEQuizAnswer3:(id)sender {
    if ([DEQuizQuestion.text isEqual: @"How many Germans speak French fluently?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer3Label.hidden = YES;
        DEQuizButtonC.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"How many Germans do not own a cell phone?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer3Label.hidden = YES;
        DEQuizButtonC.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Hans Riegel invented ..."]) {
        DEQuizResult.text = @"Correct!";
        DEQuizResult.textColor = [UIColor greenColor];
    }
    else if ([DEQuizQuestion.text isEqual: @"How many Germans speak English fluently?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer3Label.hidden = YES;
        DEQuizButtonC.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Mein name ist means ... in German."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer3Label.hidden = YES;
        DEQuizButtonC.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Guten tag means ... in German."]) {
        DEQuizResult.text = @"Correct!";
        DEQuizResult.textColor = [UIColor greenColor];
    }
    else {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer3Label.hidden = YES;
        DEQuizButtonC.hidden = YES;
    }
}

- (IBAction)DEQuizAnswer4:(id)sender {
    if ([DEQuizQuestion.text isEqual: @"How many Germans speak French fluently?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer4Label.hidden = YES;
        DEQuizButtonD.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"How many Germans do not own a cell phone?"]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer4Label.hidden = YES;
        DEQuizButtonD.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"Hans Riegel invented ..."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer4Label.hidden = YES;
        DEQuizButtonD.hidden = YES;
    }
    else if ([DEQuizQuestion.text isEqual: @"How many Germans speak English fluently?"]) {
        DEQuizResult.text = @"Correct!";
        DEQuizResult.textColor = [UIColor greenColor];
    }
    else if ([DEQuizQuestion.text isEqual: @"Mein name ist means ... in German."]) {
        DEQuizResult.text = @"Correct!";
        DEQuizResult.textColor = [UIColor greenColor];
    }
    else if ([DEQuizQuestion.text isEqual: @"Guten tag means ... in German."]) {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer4Label.hidden = YES;
        DEQuizButtonD.hidden = YES;
    }
    else {
        DEQuizResult.text = @"Wrong!";
        DEQuizResult.textColor = [UIColor redColor];
        DEQuizAnswer4Label.hidden = YES;
        DEQuizButtonD.hidden = YES;
    }
}
@end
