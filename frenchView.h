//
//  frenchView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface frenchView : UIViewController {
}
- (IBAction)doneFrench:(id)sender;
- (IBAction)randomFrench:(id)sender;
- (IBAction)switchFRDidYouKnow:(id)sender;
- (IBAction)switchFRBasic:(id)sender;
- (IBAction)switchFRQuiz:(id)sender;
@end
