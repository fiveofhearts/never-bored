//
//  romanianQuiz.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface romanianQuiz : UIViewController {
    __weak IBOutlet UITextView *TRQuizQuestion;
    __weak IBOutlet UILabel *TRQuizAnswer1Label;
    __weak IBOutlet UILabel *TRQuizAnswer2Label;
    __weak IBOutlet UILabel *TRQuizAnswer3Label;
    __weak IBOutlet UILabel *TRQuizAnswer4Label;
    __weak IBOutlet UILabel *TRQuizResult;
    __weak IBOutlet UIButton *TRQuizButtonA;
    __weak IBOutlet UIButton *TRQuizButtonB;
    __weak IBOutlet UIButton *TRQuizButtonC;
    __weak IBOutlet UIButton *TRQuizButtonD;
    __weak IBOutlet UILabel *TRQuizTut1;
    __weak IBOutlet UIButton *TRQuizTut2;
    __weak IBOutlet UILabel *TRQuizTut3;
    __weak IBOutlet UILabel *TRQuizTut4;
}
- (IBAction)doneTRQuiz:(id)sender;
- (IBAction)randomTRQuiz:(id)sender;
- (IBAction)TRQuizDidYouKnow:(id)sender;
- (IBAction)TRQuizBasic:(id)sender;
- (IBAction)TRQuizAnswer1:(id)sender;
- (IBAction)TRQuizAnswer2:(id)sender;
- (IBAction)TRQuizAnswer3:(id)sender;
- (IBAction)TRQuizAnswer4:(id)sender;

@end
