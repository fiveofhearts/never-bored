//
//  spanishBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface spanishBasic : UIViewController {
    __weak IBOutlet UIImageView *ESBasicIMGPlaceholder;
    __weak IBOutlet UILabel *ESBasicTXTPlaceholder;
    __weak IBOutlet UIButton *ESBasicPlayButton;
    __weak IBOutlet UILabel *ESBasicTut1;
    __weak IBOutlet UIButton *ESBasicTut2;
    __weak IBOutlet UILabel *ESBasicTut3;
    __weak IBOutlet UILabel *ESBasicTut4;
}
- (IBAction)doneESBasic:(id)sender;
- (IBAction)randomESBasic:(id)sender;
- (IBAction)ESBasicDidYouKnow:(id)sender;
- (IBAction)ESBasicQuiz:(id)sender;
- (IBAction)ESPlay:(id)sender;

@end
