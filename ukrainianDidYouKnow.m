//
//  ukrainianDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "ukrainianDidYouKnow.h"
#import "ukrainianBasic.h"
#import "ukrainianQuiz.h"

@interface ukrainianDidYouKnow ()

@end

@implementation ukrainianDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneUADidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomUADidYouKNow:(id)sender {
    UADidYouKnowTut1.hidden = YES;
    UADidYouKnowTut2.hidden = YES;
    UADidYouKnowTut3.hidden = YES;
    UADidYouKnowTut4.hidden = YES;
    UADidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            UADidYouKnowPlaceholder.text = @"Fact #1 \n \nThe third most visited McDonald’s in the world is located in Kiev, near the train station. This restaurant has always been in the top five most crowded McDonald’s in the world. Last year, it served 2.283.399 visitors.";
            break;
        case 1:
            UADidYouKnowPlaceholder.text = @"Fact #2 \n \nIn Europe, the police solve 30-40% of crimes, in Ukraine – 90%. This unnaturally good statistics is the result of the reluctance of the Ukrainian policemen to record “hopeless” cases (like thefts of mobile phones) and knock confessions out of suspects.";
            break;
        case 2:
            UADidYouKnowPlaceholder.text = @"Fact #3 \n \nAt the moment of the declaration of independence, Ukraine was inhabited by 19.4 million pigs, today there are only 8.3 million. Despite its reputation of a major bacon eater, an average Ukrainian eats only 18 kg of pork a year. This is three times less than a typical German eats.";
            break;
        case 3:
            UADidYouKnowPlaceholder.text = @"Fact #4 \n \nThe oldest map known to scientists, as well as the most ancient settlement of Homo Sapiens were found in Ukraine, in the village of Mezhireche. They are 14.5 – 15 thousand years old. The map is cut out of the bones of a mammoth. The settlement is made of the same material.";
            break;
        case 4:
            UADidYouKnowPlaceholder.text = @"Fact #5 \n \nThe official Guinness world record: Ukrainians made the world’s largest glass of champagne – 56.25 liters.";
            break;
        case 5:
            UADidYouKnowPlaceholder.text = @"Fact #6 \n \nThe Black Sea borders Ukraine to the south. The Dnieper is the country's largest river. The deepest river is the Danube. Yalpug is Ukraine's largest lake in the Danube delta of the Odesa Region.";
            break;
        case 6:
            UADidYouKnowPlaceholder.text = @"Fact #7 \n \nOne of the world’s heaviest silver coins is found in Ukraine. The coin named “10 years of hryvnia revival” was first produced in 2006 and weighs exactly one kilogram.";
            break;
        default:
            break;
    }
}

- (IBAction)UADidYouKnowBasic:(id)sender {
    ukrainianBasic *UABasic = [[ukrainianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UABasic animated:NO completion:NULL];
}

- (IBAction)UADidYouKnowQuiz:(id)sender {
    ukrainianQuiz *UAQuiz = [[ukrainianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:UAQuiz animated:NO completion:NULL];
}
@end
