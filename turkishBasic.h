//
//  turkishBasic.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface turkishBasic : UIViewController {
    __weak IBOutlet UIImageView *TRBasicIMGPlaceholder;
    __weak IBOutlet UILabel *TRBasicTXTPlaceholder;
    __weak IBOutlet UIButton *TRBasicPlayButton;
    __weak IBOutlet UILabel *TRBasicTut1;
    __weak IBOutlet UIButton *TRBasicTut2;
    __weak IBOutlet UILabel *TRBasicTut3;
    __weak IBOutlet UILabel *TRBasicTut4;
}
- (IBAction)doneTRBasic:(id)sender;
- (IBAction)randomTRBasic:(id)sender;
- (IBAction)TRBasicDidYouKnow:(id)sender;
- (IBAction)TRBasicQuiz:(id)sender;
- (IBAction)TRPlay:(id)sender;

@end
