//
//  danishView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/2/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "danishView.h"
#import "danishDidYouKnow.h"
#import "danishBasic.h"
#import "danishQuiz.h"

@interface danishView ()

@end

@implementation danishView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneDanish:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomDanish:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        danishDidYouKnow *DKDidYouKnow = [[danishDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:DKDidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        danishBasic *DKBasic = [[danishBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:DKBasic animated:NO completion:NULL];
    }
    else {
        danishQuiz *DKQuiz = [[danishQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:DKQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchDKDidYouKnow:(id)sender {
    danishDidYouKnow *DKDidYouKnow = [[danishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKDidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchDKBasic:(id)sender {
    danishBasic *DKBasic = [[danishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKBasic animated:NO completion:NULL];
}

- (IBAction)switchDKQuiz:(id)sender {
    danishQuiz *DKQuiz = [[danishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKQuiz animated:NO completion:NULL];
}
@end
