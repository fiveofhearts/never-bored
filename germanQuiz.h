//
//  germanQuiz.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface germanQuiz : UIViewController {
    __weak IBOutlet UITextView *DEQuizQuestion;
    __weak IBOutlet UILabel *DEQuizAnswer1Label;
    __weak IBOutlet UILabel *DEQuizAnswer2Label;
    __weak IBOutlet UILabel *DEQuizAnswer3Label;
    __weak IBOutlet UILabel *DEQuizAnswer4Label;
    __weak IBOutlet UILabel *DEQuizResult;
    __weak IBOutlet UIButton *DEQuizButtonA;
    __weak IBOutlet UIButton *DEQuizButtonB;
    __weak IBOutlet UIButton *DEQuizButtonC;
    __weak IBOutlet UIButton *DEQuizButtonD;
    __weak IBOutlet UILabel *DEQuizTut1;
    __weak IBOutlet UIButton *DEQuizTut2;
    __weak IBOutlet UILabel *DEQuizTut3;
    __weak IBOutlet UILabel *DEQuizTut4;
}
- (IBAction)doneDEQuiz:(id)sender;
- (IBAction)randomDEQuiz:(id)sender;
- (IBAction)DEQuizDidYouKnow:(id)sender;
- (IBAction)DEQuizBasic:(id)sender;
- (IBAction)DEQuizAnswer1:(id)sender;
- (IBAction)DEQuizAnswer2:(id)sender;
- (IBAction)DEQuizAnswer3:(id)sender;
- (IBAction)DEQuizAnswer4:(id)sender;

@end
