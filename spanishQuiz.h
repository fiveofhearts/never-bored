//
//  spanishQuiz.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface spanishQuiz : UIViewController {
    __weak IBOutlet UITextView *ESQuizQuestion;
    __weak IBOutlet UILabel *ESQuizAnswer1Label;
    __weak IBOutlet UILabel *ESQuizAnswer2Label;
    __weak IBOutlet UILabel *ESQuizAnswer3Label;
    __weak IBOutlet UILabel *ESQuizAnswer4Label;
    __weak IBOutlet UILabel *ESQuizResult;
    __weak IBOutlet UIButton *ESQuizButtonA;
    __weak IBOutlet UIButton *ESQuizButtonB;
    __weak IBOutlet UIButton *ESQuizButtonC;
    __weak IBOutlet UIButton *ESQuizButtonD;
    __weak IBOutlet UILabel *ESQuizTut1;
    __weak IBOutlet UIButton *ESQuizTut2;
    __weak IBOutlet UILabel *ESQuizTut3;
    __weak IBOutlet UILabel *ESQuizTut4;
}
- (IBAction)doneESQuiz:(id)sender;
- (IBAction)randomESQuiz:(id)sender;
- (IBAction)ESQuizDidYouKnow:(id)sender;
- (IBAction)ESQuizBasic:(id)sender;
- (IBAction)ESQuizAnswer1:(id)sender;
- (IBAction)ESQuizAnswer2:(id)sender;
- (IBAction)ESQuizAnswer3:(id)sender;
- (IBAction)ESQuizAnswer4:(id)sender;

@end
