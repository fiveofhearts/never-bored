//
//  romanianQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "romanianQuiz.h"
#import "romanianDidYouKnow.h"
#import "romanianBasic.h"

@interface romanianQuiz ()

@end

@implementation romanianQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTRQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomTRQuiz:(id)sender {
    TRQuizTut1.hidden = YES;
    TRQuizTut2.hidden = YES;
    TRQuizTut3.hidden = YES;
    TRQuizTut4.hidden = YES;
    TRQuizQuestion.hidden = NO;
    TRQuizButtonA.hidden = NO;
    TRQuizButtonB.hidden = NO;
    TRQuizButtonC.hidden = NO;
    TRQuizButtonD.hidden = NO;
    TRQuizAnswer1Label.hidden = NO;
    TRQuizAnswer2Label.hidden = NO;
    TRQuizAnswer3Label.hidden = NO;
    TRQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            TRQuizQuestion.text = @"The Scarisoara glacier existed for more than ... years.";
            TRQuizAnswer1Label.text = @"3,500";
            TRQuizAnswer2Label.text = @"2,000";
            TRQuizAnswer3Label.text = @"2,500";
            TRQuizAnswer4Label.text = @"3,000";
            TRQuizResult.text = @"";
            break;
        case 1:
            TRQuizQuestion.text = @"The scientist who discovered ... was Nicolae Paulescu.";
            TRQuizAnswer1Label.text = @"X-rays";
            TRQuizAnswer2Label.text = @"Insulin";
            TRQuizAnswer3Label.text = @"Penicillin";
            TRQuizAnswer4Label.text = @"Anesthesia";
            TRQuizResult.text = @"";
            break;
        case 2:
            TRQuizQuestion.text = @"The first ever score of ... in gymnastics was given to Romanian gymnast Nadia Comaneci.";
            TRQuizAnswer1Label.text = @"9.7";
            TRQuizAnswer2Label.text = @"9.8";
            TRQuizAnswer3Label.text = @"10";
            TRQuizAnswer4Label.text = @"9.9";
            TRQuizResult.text = @"";
            break;
        case 3:
            TRQuizQuestion.text = @"The city of Timisoara in Romania is the birth place of the electric street light. This happened in ...";
            TRQuizAnswer1Label.text = @"1873";
            TRQuizAnswer2Label.text = @"1878";
            TRQuizAnswer3Label.text = @"1895";
            TRQuizAnswer4Label.text = @"1889";
            TRQuizResult.text = @"";
            break;
        case 4:
            TRQuizQuestion.text = @"Mă numesc means ... in Romanian.";
            TRQuizAnswer1Label.text = @"Hello";
            TRQuizAnswer2Label.text = @"Goodbye";
            TRQuizAnswer3Label.text = @"Yes";
            TRQuizAnswer4Label.text = @"My name is";
            TRQuizResult.text = @"";
            break;
        case 5:
            TRQuizQuestion.text = @"Salut means ... in Romanian.";
            TRQuizAnswer1Label.text = @"Goodbye";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Hello";
            TRQuizAnswer4Label.text = @"No";
            TRQuizResult.text = @"";
            break;
        case 6:
            TRQuizQuestion.text = @"Mulţumesc means ... in Romanian.";
            TRQuizAnswer1Label.text = @"Yes";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Goodbye";
            TRQuizAnswer4Label.text = @"Hello";
            TRQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)TRQuizDidYouKnow:(id)sender {
    romanianDidYouKnow *RODidYouKnow = [[romanianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RODidYouKnow animated:NO completion:NULL];
}

- (IBAction)TRQuizBasic:(id)sender {
    romanianBasic *ROBasic = [[romanianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ROBasic animated:NO completion:NULL];
}

- (IBAction)TRQuizAnswer1:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"The Scarisoara glacier existed for more than ... years."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"The scientist who discovered ... was Nicolae Paulescu."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The first ever score of ... in gymnastics was given to Romanian gymnast Nadia Comaneci."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The city of Timisoara in Romania is the birth place of the electric street light. This happened in ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Mă numesc means ... in Romanian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Salut means ... in Romanian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer2:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"The Scarisoara glacier existed for more than ... years."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The scientist who discovered ... was Nicolae Paulescu."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"The first ever score of ... in gymnastics was given to Romanian gymnast Nadia Comaneci."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The city of Timisoara in Romania is the birth place of the electric street light. This happened in ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Mă numesc means ... in Romanian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Salut means ... in Romanian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)TRQuizAnswer3:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"The Scarisoara glacier existed for more than ... years."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The scientist who discovered ... was Nicolae Paulescu."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The first ever score of ... in gymnastics was given to Romanian gymnast Nadia Comaneci."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"The city of Timisoara in Romania is the birth place of the electric street light. This happened in ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Mă numesc means ... in Romanian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Salut means ... in Romanian."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer4:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"The Scarisoara glacier existed for more than ... years."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The scientist who discovered ... was Nicolae Paulescu."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The first ever score of ... in gymnastics was given to Romanian gymnast Nadia Comaneci."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The city of Timisoara in Romania is the birth place of the electric street light. This happened in ..."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Mă numesc means ... in Romanian."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Salut means ... in Romanian."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
}
@end
