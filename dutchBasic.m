//
//  dutchBasic.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "dutchBasic.h"
#import "dutchDidYouKnow.h"
#import "dutchQuiz.h"
#import <AudioToolbox/AudioToolbox.h>

@interface dutchBasic ()

@end

@implementation dutchBasic

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneNLBasic:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomNLBasic:(id)sender {
    NLBasicTut1.hidden = YES;
    NLBasicTut2.hidden = YES;
    NLBasicTut3.hidden = YES;
    NLBasicTut4.hidden = YES;
    NLBasicIMGPlaceholder.hidden = NO;
    NLBasicPlayButton.hidden = NO;
    NLBasicTXTPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            NLBasicIMGPlaceholder.image = [UIImage imageNamed:@"goodbye.png"];
            NLBasicTXTPlaceholder.text = @"Dag";
            break;
        case 1:
            NLBasicIMGPlaceholder.image = [UIImage imageNamed:@"hi.png"];
            NLBasicTXTPlaceholder.text = @"Hoi";
            break;
        case 2:
            NLBasicIMGPlaceholder.image = [UIImage imageNamed:@"myNameIs.png"];
            NLBasicTXTPlaceholder.text = @"Ik heet";
            break;
        case 3:
            NLBasicIMGPlaceholder.image = [UIImage imageNamed:@"niceToMeetYou.png"];
            NLBasicTXTPlaceholder.text = @"Prettig met u kennis te maken";
            break;
        case 4:
            NLBasicIMGPlaceholder.image = [UIImage imageNamed:@"no.png"];
            NLBasicTXTPlaceholder.text = @"Nee";
            break;
        case 5:
            NLBasicIMGPlaceholder.image = [UIImage imageNamed:@"thankYou.png"];
            NLBasicTXTPlaceholder.text = @"Dank u";
            break;
        case 6:
            NLBasicIMGPlaceholder.image = [UIImage imageNamed:@"yes.png"];
            NLBasicTXTPlaceholder.text = @"Ja";
            break;
        default:
            break;
    }
}

- (IBAction)NLBasicDidYouKnow:(id)sender {
    dutchDidYouKnow *NLDidYouKnow = [[dutchDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:NLDidYouKnow animated:NO completion:NULL];
}

- (IBAction)NLBasicQuiz:(id)sender {
    dutchQuiz *NLQuiz = [[dutchQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:NLQuiz animated:NO completion:NULL];
}

- (IBAction)NLPlay:(id)sender {
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    if ([NLBasicTXTPlaceholder.text isEqual: @"Dag"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"goodbyeDutch", CFSTR ("mp3"), NULL);
    }
    else if ([NLBasicTXTPlaceholder.text isEqual: @"Hoi"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"hiDutch", CFSTR ("mp3"), NULL);
    }
    else if ([NLBasicTXTPlaceholder.text isEqual: @"Ik heet"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"myNameIsDutch", CFSTR ("mp3"), NULL);
    }
    else if ([NLBasicTXTPlaceholder.text isEqual: @"Prettig met u kennis te maken"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"niceToMeetYouDutch", CFSTR ("mp3"), NULL);
    }
    else if ([NLBasicTXTPlaceholder.text isEqual: @"Nee"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"noDutch", CFSTR ("mp3"), NULL);
    }
    else if ([NLBasicTXTPlaceholder.text isEqual: @"Dank u"]) {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"thankYouDutch", CFSTR ("mp3"), NULL);
    }
    else {
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"yesDutch", CFSTR ("mp3"), NULL);
    }
    
    UInt32 soundID;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
@end
