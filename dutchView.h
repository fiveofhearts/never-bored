//
//  dutchView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dutchView : UIViewController {
}
- (IBAction)doneDutch:(id)sender;
- (IBAction)randomDutch:(id)sender;
- (IBAction)switchNLDidYouKnow:(id)sender;
- (IBAction)switchNLBasic:(id)sender;
- (IBAction)switchNLQuiz:(id)sender;
@end
