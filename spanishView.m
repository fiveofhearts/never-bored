//
//  spanishView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "spanishView.h"
#import "spanishDidYouKnow.h"
#import "spanishBasic.h"
#import "spanishQuiz.h"

@interface spanishView ()

@end

@implementation spanishView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneSpanish:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomSpanish:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        spanishDidYouKnow *ESDidYouKnow = [[spanishDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:ESDidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        spanishBasic *ESBasic = [[spanishBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:ESBasic animated:NO completion:NULL];
    }
    else {
        spanishQuiz *ESQuiz = [[spanishQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:ESQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchESDidYouKnow:(id)sender {
    spanishDidYouKnow *ESDidYouKnow = [[spanishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESDidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchESBasic:(id)sender {
    spanishBasic *ESBasic = [[spanishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESBasic animated:NO completion:NULL];
}

- (IBAction)switchESQuiz:(id)sender {
    spanishQuiz *ESQuiz = [[spanishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ESQuiz animated:NO completion:NULL];
}
@end
