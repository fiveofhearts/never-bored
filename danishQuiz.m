//
//  danishQuiz.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "danishQuiz.h"
#import "danishDidYouKnow.h"
#import "danishBasic.h"

@interface danishQuiz ()

@end

@implementation danishQuiz

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneTRQuiz:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomTRQuiz:(id)sender {
    TRQuizTut1.hidden = YES;
    TRQuizTut2.hidden = YES;
    TRQuizTut3.hidden = YES;
    TRQuizTut4.hidden = YES;
    TRQuizQuestion.hidden = NO;
    TRQuizButtonA.hidden = NO;
    TRQuizButtonB.hidden = NO;
    TRQuizButtonC.hidden = NO;
    TRQuizButtonD.hidden = NO;
    TRQuizAnswer1Label.hidden = NO;
    TRQuizAnswer2Label.hidden = NO;
    TRQuizAnswer3Label.hidden = NO;
    TRQuizAnswer4Label.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            TRQuizQuestion.text = @"Danish men marry the oldest of all Europeans - at ... old in average.";
            TRQuizAnswer1Label.text = @"32 years";
            TRQuizAnswer2Label.text = @"36 years";
            TRQuizAnswer3Label.text = @"41 years";
            TRQuizAnswer4Label.text = @"47 years";
            TRQuizResult.text = @"";
            break;
        case 1:
            TRQuizQuestion.text = @"The country's average height above sea level is only ...";
            TRQuizAnswer1Label.text = @"12 meters";
            TRQuizAnswer2Label.text = @"31 meters";
            TRQuizAnswer3Label.text = @"24 meters";
            TRQuizAnswer4Label.text = @"49 meters";
            TRQuizResult.text = @"";
            break;
        case 2:
            TRQuizQuestion.text = @"Carlsberg, Tuborg and ... are danish companies.";
            TRQuizAnswer1Label.text = @"Nokia";
            TRQuizAnswer2Label.text = @"Volvo";
            TRQuizAnswer3Label.text = @"Lego";
            TRQuizAnswer4Label.text = @"H&M";
            TRQuizResult.text = @"";
            break;
        case 3:
            TRQuizQuestion.text = @"Gorm the Old, the first king of the 'official line', ruled from ... C.E.";
            TRQuizAnswer1Label.text = @"780";
            TRQuizAnswer2Label.text = @"830";
            TRQuizAnswer3Label.text = @"1024";
            TRQuizAnswer4Label.text = @"934";
            TRQuizResult.text = @"";
            break;
        case 4:
            TRQuizQuestion.text = @"Jeg hedder means ... in Danish.";
            TRQuizAnswer1Label.text = @"Hello";
            TRQuizAnswer2Label.text = @"Goodbye";
            TRQuizAnswer3Label.text = @"Yes";
            TRQuizAnswer4Label.text = @"My name is";
            TRQuizResult.text = @"";
            break;
        case 5:
            TRQuizQuestion.text = @"Goddag means ... in Danish.";
            TRQuizAnswer1Label.text = @"Goodbye";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Hello";
            TRQuizAnswer4Label.text = @"No";
            TRQuizResult.text = @"";
            break;
        case 6:
            TRQuizQuestion.text = @"Tak means ... in Danish.";
            TRQuizAnswer1Label.text = @"Yes";
            TRQuizAnswer2Label.text = @"Thank you";
            TRQuizAnswer3Label.text = @"Goodbye";
            TRQuizAnswer4Label.text = @"Hello";
            TRQuizResult.text = @"";
            break;
        default:
            break;
    }
    
}

- (IBAction)TRQuizDidYouKnow:(id)sender {
    danishDidYouKnow *DKDidYouKnow = [[danishDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKDidYouKnow animated:NO completion:NULL];
}

- (IBAction)TRQuizBasic:(id)sender {
    danishBasic *DKBasic = [[danishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:DKBasic animated:NO completion:NULL];
}

- (IBAction)TRQuizAnswer1:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Danish men marry the oldest of all Europeans - at ... old in average."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"The country's average height above sea level is only ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Carlsberg, Tuborg and ... are danish companies."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Gorm the Old, the first king of the 'official line', ruled from ... C.E."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Jeg hedder means ... in Danish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Goddag means ... in Danish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer1Label.hidden = YES;
        TRQuizButtonA.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer2:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Danish men marry the oldest of all Europeans - at ... old in average."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The country's average height above sea level is only ..."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Carlsberg, Tuborg and ... are danish companies."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Gorm the Old, the first king of the 'official line', ruled from ... C.E."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Jeg hedder means ... in Danish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Goddag means ... in Danish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer2Label.hidden = YES;
        TRQuizButtonB.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
}

- (IBAction)TRQuizAnswer3:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Danish men marry the oldest of all Europeans - at ... old in average."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The country's average height above sea level is only ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Carlsberg, Tuborg and ... are danish companies."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Gorm the Old, the first king of the 'official line', ruled from ... C.E."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Jeg hedder means ... in Danish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Goddag means ... in Danish."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer3Label.hidden = YES;
        TRQuizButtonC.hidden = YES;
    }
}

- (IBAction)TRQuizAnswer4:(id)sender {
    if ([TRQuizQuestion.text isEqual: @"Danish men marry the oldest of all Europeans - at ... old in average."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"The country's average height above sea level is only ..."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Carlsberg, Tuborg and ... are danish companies."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else if ([TRQuizQuestion.text isEqual: @"Gorm the Old, the first king of the 'official line', ruled from ... C.E."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Jeg hedder means ... in Danish."]) {
        TRQuizResult.text = @"Correct!";
        TRQuizResult.textColor = [UIColor greenColor];
    }
    else if ([TRQuizQuestion.text isEqual: @"Goddag means ... in Danish."]) {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
    else {
        TRQuizResult.text = @"Wrong!";
        TRQuizResult.textColor = [UIColor redColor];
        TRQuizAnswer4Label.hidden = YES;
        TRQuizButtonD.hidden = YES;
    }
}
@end
