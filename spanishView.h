//
//  spanishView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface spanishView : UIViewController {
}
- (IBAction)doneSpanish:(id)sender;
- (IBAction)randomSpanish:(id)sender;
- (IBAction)switchESDidYouKnow:(id)sender;
- (IBAction)switchESBasic:(id)sender;
- (IBAction)switchESQuiz:(id)sender;
@end
