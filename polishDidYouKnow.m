//
//  polishDidYouKnow.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "polishDidYouKnow.h"
#import "polishBasic.h"
#import "polishQuiz.h"

@interface polishDidYouKnow ()

@end

@implementation polishDidYouKnow

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)donePLDidYouKnow:(id)sender {
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)randomPLDidYouKNow:(id)sender {
    PLDidYouKnowTut1.hidden = YES;
    PLDidYouKnowTut2.hidden = YES;
    PLDidYouKnowTut3.hidden = YES;
    PLDidYouKnowTut4.hidden = YES;
    PLDidYouKnowPlaceholder.hidden = NO;
    int i = rand() %7;
    switch (i) {
        case 0:
            PLDidYouKnowPlaceholder.text = @"Fact #1 \n \nGeographically, Poland is not actually in Eastern Europe – it is in fact in the very center of Europe.";
            break;
        case 1:
            PLDidYouKnowPlaceholder.text = @"Fact #2 \n \nPoland’s national symbol is the white-tailed Eagle.";
            break;
        case 2:
            PLDidYouKnowPlaceholder.text = @"Fact #3 \n \nOne third of Poland is covered with forest, 50% of the land is dedicated to farming, there are a total of 9300 lakes, 23 National Parks and only one desert.";
            break;
        case 3:
            PLDidYouKnowPlaceholder.text = @"Fact #4 \n \nPoland has had many capital cities in its time. These have included Gniezno, Poznan, Krakow and Warsaw. Lublin has been the capital twice – after both World Wars.";
            break;
        case 4:
            PLDidYouKnowPlaceholder.text = @"Fact #5 \n \nThe most popular dog’s name in Poland is “Burek” which is actually the Polish word for a brown-grey colour.";
            break;
        case 5:
            PLDidYouKnowPlaceholder.text = @"Fact #6 \n \nIn Poland the name day is considered more important than the birthday.";
            break;
        case 6:
            PLDidYouKnowPlaceholder.text = @"Fact #7 \n \nSome Polish beer is 10% alcohol.";
            break;
        default:
            break;
    }
}

- (IBAction)PLDidYouKnowBasic:(id)sender {
    polishBasic *PLBasic = [[polishBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLBasic animated:NO completion:NULL];
}

- (IBAction)PLDidYouKnowQuiz:(id)sender {
    polishQuiz *PLQuiz = [[polishQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:PLQuiz animated:NO completion:NULL];
}
@end
