//
//  romanianView.m
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import "romanianView.h"
#import "romanianDidYouKnow.h"
#import "romanianBasic.h"
#import "romanianQuiz.h"

@interface romanianView ()

@end

@implementation romanianView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneRomanian:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)randomRomanian:(id)sender {
    int i = arc4random() % 3;
    if (i == 1) {
        romanianDidYouKnow *RODidYouKnow = [[romanianDidYouKnow alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:RODidYouKnow animated:NO completion:NULL];
    }
    else if (i == 2) {
        romanianBasic *ROBasic = [[romanianBasic alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:ROBasic animated:NO completion:NULL];
    }
    else {
        romanianQuiz *ROQuiz = [[romanianQuiz alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:ROQuiz animated:NO completion:NULL];
    }
}

- (IBAction)switchRODidYouKnow:(id)sender {
    romanianDidYouKnow *RODidYouKnow = [[romanianDidYouKnow alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:RODidYouKnow animated:NO completion:NULL];
}

- (IBAction)switchROBasic:(id)sender {
    romanianBasic *ROBasic = [[romanianBasic alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ROBasic animated:NO completion:NULL];
}

- (IBAction)switchROQuiz:(id)sender {
    romanianQuiz *ROQuiz = [[romanianQuiz alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:ROQuiz animated:NO completion:NULL];
}
@end
