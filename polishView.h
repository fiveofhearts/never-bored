//
//  polishView.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/1/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface polishView : UIViewController {
}
- (IBAction)donePolish:(id)sender;
- (IBAction)randomPolish:(id)sender;
- (IBAction)switchPLDidYouKnow:(id)sender;
- (IBAction)switchPLBasic:(id)sender;
- (IBAction)switchPLQuiz:(id)sender;
@end
