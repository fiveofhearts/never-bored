//
//  russianDidYouKnow.h
//  Never Bored
//
//  Created by Marius-Nicolae Vaduva on 10/3/13.
//  Copyright (c) 2013 Marius-Nicolae Vaduva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface russianDidYouKnow : UIViewController {
    __weak IBOutlet UITextView *RUDidYouKnowPlaceholder;
    __weak IBOutlet UILabel *RUDidYouKnowTut1;
    __weak IBOutlet UIButton *RUDidYouKnowTut2;
    __weak IBOutlet UILabel *RUDidYouKnowTut3;
    __weak IBOutlet UILabel *RUDidYouKnowTut4;
    
}
- (IBAction)doneRUDidYouKnow:(id)sender;
- (IBAction)randomRUDidYouKNow:(id)sender;
- (IBAction)RUDidYouKnowBasic:(id)sender;
- (IBAction)RUDidYouKnowQuiz:(id)sender;
@end
